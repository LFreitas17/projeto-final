import React from 'react';
import {Button, Modal, Form, Input, Icon, Select, InputNumber} from 'antd';
import {updateCargo} from './Requests';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
        }

        onChange = (e) => {
            console.log('radio checked', e.target.value);
            console.log('data', this.props.data);
            this.setState({
                value: e.target.value,
            });
        }

        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;
            const cargoInfo = this.props.data;

            return (
                <Modal
                    visible={visible}
                    title="Editar Carga"
                    okText="Edit"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Número da Guia">
                            {getFieldDecorator('numberGuide', {
                                initialValue: cargoInfo.numberGuide,
                                rules: [{required: true, message: 'Por favor insira o número da guia!'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Receção">
                            {getFieldDecorator('receptionDate', {
                                initialValue: cargoInfo.receptionDate,
                                rules: [{required: true, message: 'Por favor insira a data de receção! DD/MM/YYYY'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Peso">
                            {getFieldDecorator('weight', {
                                initialValue: cargoInfo.weight,
                                rules: [{required: true, message: 'Por favor insira o peso da carga!'}],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Preço">
                            {getFieldDecorator('price', {
                                initialValue: cargoInfo.price,
                                rules: [{required: true, message: 'Por favor insira o preço da carga!'}],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Carga">
                            {getFieldDecorator('type', {
                                initialValue: cargoInfo.type,
                                rules: [{ required: true, message: 'Por favor escolha o tipo da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="EUROS_TON">Por Tonelada</Option>
                                    <Option value="EUROS_MCUB">Por m³</Option>
                                    <Option value="EUROS_LOAD" >Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Compra">
                            {getFieldDecorator('purchaseType', {
                                initialValue: cargoInfo.purchaseType,
                                rules: [{ required: true, message: 'Por favor escolha o tipo de compra!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="BY_BLOCK">Por Bloco</Option>
                                    <Option value="BY_LOAD">Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Estado da Encomenda">
                            {getFieldDecorator('state', {
                                initialValue: cargoInfo.state,
                                rules: [{ required: true, message: 'Por favor escolha o estado da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="NORMAL">Normal</Option>
                                    <Option value="NOT_FINISHED">Por Acabar</Option>
                                    <Option value="FINISHED">Concluida</Option>
                                    <Option value="VERIFIED">Verificada</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Nome Encomenda">
                            {getFieldDecorator('nameOrder', {
                                initialValue: cargoInfo.nameOrder,
                                rules: [{required: false, message: 'Por favor insira o nome da encomenda!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class EditCargo extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        const cargoId = this.props.cargoId;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            updateCargo(values, cargoId, this.props.onEdit);

            form.resetFields();
            this.setState({visible: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {

        return (
            <span>
        <Button onClick={this.showModal} disabled={this.props.disable}
                loading={this.props.loading}><Icon type="edit" theme="outlined"/>Editar Carga</Button>
          <CollectionCreateForm data={this.props.data}
                                wrappedComponentRef={this.saveFormRef}
                                visible={this.state.visible}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
          />
      </span>
        );
    }
}

export default EditCargo;

import React, { Component } from 'react';
import {Form, Table } from 'antd';
import HorizontalForm from './HorizontalForm.js';
import DeleteAllocationP from './DeleteAllocationP.js';
import { getProjectEmployees } from './Requests.js';

const WrappedHorizontalLoginForm = Form.create()(HorizontalForm);

function compareByAlph(a, b) { if (a > b) { return -1; } if (a < b) { return 1; } return 0; }

class ProjectAllocation extends Component {

    constructor() {
        super();
        this.state = {
            employees: [],
        }
        this.saveList=this.saveList.bind(this);
    }

    saveList(response) {
        this.setState({employees:response});
    }

    componentDidMount() {
        const projectId = this.props.projectId;
        getProjectEmployees(projectId, this.saveList);
    }

    render() {
        const employeeList = this.state.employees || [];

        const columns = [{
            title: 'Block',
            dataIndex: 'name',
            sorter: (a, b) => compareByAlph(a.name, b.name)
        }, {
            title: 'Cargo',
            dataIndex: 'association',
            sorter: (a, b) => compareByAlph(a.startDate, b.startDate)
        }, {
            render: (text, record) => (
                <span>
                    <DeleteAllocationP projectId={this.props.projectId} employeeId={record.employeeId} onChange={this.saveList}/>
                </span>
            )}];

        return (
            <div><Table rowKey={record => record.employeeId} dataSource={employeeList} columns={columns} />
                <WrappedHorizontalLoginForm created={this.saveList} projectId={this.props.projectId} />
            </div>
        );
    }

}
export default ProjectAllocation;

import React, { Component } from 'react';
import CollectionsPage from './GraniteEdit.js';
import { getAllGranites } from './Requests.js';

class Granite extends Component {

    constructor() {
        super();
        this.state = {
            granites: [],
        }

        this.saveList=this.saveList.bind(this);
    }

    componentDidMount() {
        getAllGranites(this.saveList);
    }

    saveList(response){
        this.setState({ granites: response });
    }

    render() {
        const graniteList = this.state.granites || [];
        return (
            <div >
                <CollectionsPage created={this.saveList}  graniteList={graniteList}/>
            </div>
        );
    }
}
export default Granite;
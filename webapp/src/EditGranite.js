import React from 'react';
import {Button, Form, Icon, Input, InputNumber, Modal} from 'antd';
import {updateGranite} from './Requests';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
        }

        onChange = (e) => {
            console.log('radio checked', e.target.value);
            console.log('data', this.props.data);
            this.setState({
                value: e.target.value,
            });
        }

        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;
            const graniteInfo = this.props.data;

            return (
                <Modal
                    visible={visible}
                    title="Editar Granito"
                    okText="Edit"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Familia do granito">
                            {getFieldDecorator('family', {
                                initialValue: graniteInfo.family,
                                rules: [{required: true, message: 'Por favor insira a familia do granito!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="Tipo de Granito">
                            {getFieldDecorator('type', {
                                initialValue: graniteInfo.type,
                                rules: [{required: true, message: 'Por favor insira o tipo do granito!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="Preço (€)">
                            {getFieldDecorator('price', {
                                initialValue: graniteInfo.price,
                                rules: [{required: true, message: 'Por favor insira o preço do granito!'}],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class EditGranite extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        const graniteId = this.props.graniteId;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            updateGranite(values, graniteId, this.props.onEdit);

            form.resetFields();
            this.setState({visible: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {

        return (
            <span>
        <Button onClick={this.showModal} disabled={this.props.disable}
                loading={this.props.loading}><Icon type="edit" theme="outlined"/>Editar Granito</Button>
          <CollectionCreateForm data={this.props.data}
                                wrappedComponentRef={this.saveFormRef}
                                visible={this.state.visible}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
          />
      </span>
        );
    }
}

export default EditGranite;

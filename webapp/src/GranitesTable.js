import React, {Component} from 'react';
import {Table, Button, Modal, Icon} from 'antd';
import EditGranite from './EditGranite.js';
import {deleteGranite} from './Requests.js';
import './GranitesTable.css';

function compareByAlph(a, b) {
    if (a > b) {
        return -1;
    }
    if (a < b) {
        return 1;
    }
    return 0;
}

class GranitesTable extends Component {
    state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        visible: false,
        loadingDelete: false,
        graniteInfo: {}
    };

    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
                select: true,
            });
        }, 1000);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({loadingDelete: true});
        const graniteId = this.state.selectedRowKeys[0];

        deleteGranite(graniteId, this.props.onCreated);

        setTimeout(() => {
            this.setState({loadingDelete: false, visible: false, selectedRowKeys: []});
        }, 500);
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});

    }

    selectRow = record => {
        this.setState({
            graniteInfo: record
        })
    }

    render() {
        const dataSource = this.props.data;
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const hasSelected = selectedRowKeys.length === 1;

        const columns = [{
            title: 'Familia do granito',
            dataIndex: 'family',
            sorter: (a, b) => compareByAlph(a.family, b.family)
        }, {
            title: 'Tipo de Granito',
            dataIndex: 'type',
            sorter: (a, b) => compareByAlph(a.type, b.type)
        }, {
            title: 'Preço (€)',
            dataIndex: 'price',
            sorter: (a, b) => compareByAlph(a.price, b.price)
        }];

        const {visible, loadingDelete} = this.state;
        const graniteId = this.state.selectedRowKeys[0];

        return (
            <span>
                <div className="divider_button_delete"></div>
                    <EditGranite data={this.state.graniteInfo} onEdit={this.props.onCreated} disable={!hasSelected}
                               loading={loading} graniteId={graniteId}/>
                <div className="divider_button_delete"></div>
                <Button disabled={!hasSelected}
                        loading={loading} onClick={this.showModal}><Icon type="minus" theme="outlined"/>Eliminar Granito</Button>
                <Modal
                    title="Eliminar Granito"
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={loadingDelete} onClick={this.handleOk}>
                            Delete
                        </Button>,
                    ]}>

                    <p>Tem a certeza que quer apagar o granito?</p>
                </Modal>


                <div className="layout_employee_table">

                    <Table rowKey={record => record.id} rowSelection={rowSelection} dataSource={dataSource}
                           columns={columns}
                           onRow={record => ({
                               onChange: () => {
                                   this.selectRow(record);
                               }
                           })}/>

                </div>
            </span>);
    }

}

export default GranitesTable;

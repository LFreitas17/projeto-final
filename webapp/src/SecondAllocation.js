import React from 'react';
import {Modal, Button, Popover } from 'antd';
import AddAllocation from './AddAllocation.js';


class ProjectEmployees extends React.Component {
    state = {
        loading: false,
        visible: false,
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({ loading: true });
        setTimeout(() => {
            this.setState({ loading: false, visible: false });
        }, 1000);
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }


    render() {
        const { visible, loading } = this.state;

        return (
            <div>
                <Popover content="View Details">
                    <Button onClick={this.showModal}>
                        Employees
                    </Button>
                </Popover>
                <Modal
                    visible={visible}
                    title="Project Employees"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>Save</Button>,
                    ]}
                >
                    <p>Project: Project {this.props.projectId}</p>

                    <AddAllocation projectId={this.props.projectId} />
                </Modal>
            </div>
                );
            }
        }

        
export default ProjectEmployees;
import React, { Component } from 'react';
import { Form, Table } from 'antd';
import EmployeeHorizontalForm from './EmployeeHorizontalForm.js';
import DeleteAllocationE from './DeleteAllocationE.js';
import { getEmployeeProjects } from './Requests.js';

const WrappedLoginForm = Form.create()(EmployeeHorizontalForm);

function compareByAlph(a, b) { if (a > b) { return -1; } if (a < b) { return 1; } return 0; }

class EmployeeAllocation extends Component {

    constructor() {
        super();
        this.state = {
            projects: [],
        }
        this.saveList = this.saveList.bind(this);
    }

    saveList(response) {
        this.setState({ projects: response })
    }

    componentDidMount() {
        const employeeId = this.props.employeeId;
        getEmployeeProjects(employeeId, this.saveList);
    }

    render() {
        const projectList = this.state.projects || [];

        const columns = [{
            title: 'Cargo',
            dataIndex: 'name',
            sorter: (a, b) => compareByAlph(a.name, b.name)
        }, {
            title: 'Block',
            dataIndex: 'association',
            sorter: (a, b) => compareByAlph(a.startDate, b.startDate)
        }, {
            render: (text, record) => (
                <span>
                    <DeleteAllocationE projectId={record.projectId} employeeId={this.props.employeeId} onChange={this.saveList} />
                </span>
            )
        }];

        return (
            <div><Table rowKey={record => record.projectId} dataSource={projectList} columns={columns} />
                <WrappedLoginForm created={this.saveList} employeeId={this.props.employeeId} />
            </div>
        );
    }

}
export default EmployeeAllocation;

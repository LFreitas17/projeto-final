import React from 'react';
import {Button, Modal, Form, Input, Icon, Select, InputNumber} from 'antd';
import {updateBlock} from './Requests';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
        }

        onChange = (e) => {
            console.log('radio checked', e.target.value);
            console.log('data', this.props.data);
            this.setState({
                value: e.target.value,
            });
        }

        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;
            const blockInfo = this.props.data;

            return (
                <Modal
                    visible={visible}
                    title="Editar Bloco"
                    okText="Edit"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Número do Bloco">
                            {getFieldDecorator('blockNumber', {
                                initialValue: blockInfo.blockNumber,
                                rules:[{required: true, message: 'Por favor insira o número do bloco!'}]
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>

                        <FormItem label="Altura">
                            {getFieldDecorator('height', {
                                initialValue: blockInfo.height,
                                rules: [{required: true, message: 'Por favor insira a altura do bloco!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>

                        <FormItem label="Comprimento">
                            {getFieldDecorator('length', {
                                initialValue: blockInfo.length,
                                rules: [{required: true, message: 'Por favor insira o comprimento do bloco!'}],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>

                        <FormItem label="Largura">
                            {getFieldDecorator('width', {
                                initialValue: blockInfo.width,
                                rules: [{required: true, message: 'Por favor insira a largura do bloco!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>

                        <FormItem label="Qualidade do bloco">
                            {getFieldDecorator('quality', {
                                initialValue: blockInfo.quality,
                                rules: [{required: true, message: 'Por favor escolha a qualidade que está o bloco!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="GOOD">BOM</Option>
                                    <Option value="BAD">MAU</Option>
                                </Select>
                            )}
                        </FormItem>

                        <FormItem label="Block Rating">
                            {getFieldDecorator('rating', {
                                initialValue: blockInfo.rating,
                                rules: [{ required: true, message: 'Por favor escolha a avaliação devida para o bloco!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="OK">OK</Option>
                                    <Option value="NOT_OK">NOT_OK</Option>
                                </Select>
                            )}
                        </FormItem>

                        <FormItem label="Block Description">
                            {getFieldDecorator('description', {
                                initialValue: blockInfo.description,
                                rules: [{ required: false, message: 'Por favor insira uma descrição!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="Número da guia">
                            {getFieldDecorator('numberOfGuide', {
                                initialValue: blockInfo.numberOfGuide,
                                rules: [{ required: false, message: 'Por favor insira o número da guia da carga que este bloco esta associado!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class EditBlock extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        const blockId = this.props.blockId;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            updateBlock(values, blockId, this.props.onEdit);

            form.resetFields();
            this.setState({visible: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {

        return (
            <span>
        <Button onClick={this.showModal} disabled={this.props.disable}
                loading={this.props.loading}><Icon type="edit" theme="outlined"/>Editar Bloco</Button>
          <CollectionCreateForm data={this.props.data}
                                wrappedComponentRef={this.saveFormRef}
                                visible={this.state.visible}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
          />
      </span>
        );
    }
}

export default EditBlock;

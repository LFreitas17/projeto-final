import React, {Component} from 'react';
import {Table, Icon, Modal, Button} from 'antd';
import EditUser from './EditUser.js';
import './UsersTable.css';
import {deleteUser} from './Requests.js';

function compareByAlph(a, b) {
    if (a > b) {
        return -1;
    }
    if (a < b) {
        return 1;
    }
    return 0;
}

class UserTable extends Component {
    state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        visible: false,
        loadingDelete: false,
        employeeInfo: {}
    };

    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
                select: true,
            });
        }, 1000);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({loadingDelete: true});
        const userId = this.state.selectedRowKeys[0];

        deleteUser(userId, this.props.onChange);

        setTimeout(() => {
            this.setState({loadingDelete: false, visible: false, selectedRowKeys: []});
        }, 1000);
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }


    onSelectChange = (selectedRowKeys) => {
        this.setState({selectedRowKeys});
    }

    selectRow = record => {
        this.setState({
            employeeInfo: record
        })
    }


    render() {
        const dataSource = this.props.data;
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const hasSelected = selectedRowKeys.length === 1;

        const columns = [{
            title: 'Nome',
            dataIndex: 'username',
            sorter: (a, b) => compareByAlph(a.username, b.username)
        }, {
            title: 'Telemóvel',
            dataIndex: 'telephone',
            sorter: (a, b) => compareByAlph(a.telephone, b.telephone)
        }, {
            title: 'Morada',
            dataIndex: 'address',
            sorter: (a, b) => compareByAlph(a.address, b.address)
        }];

        const {visible, loadingDelete} = this.state;
        const selectedUser = this.state.selectedRowKeys[0];

        return (
            <span>
                <div className="divider_button_delete"></div>
                    <EditUser data={this.state.employeeInfo} onEdit={this.props.onChange} disable={!hasSelected}
                                  loading={loading} selectedUser={selectedUser}/>
                <div className="divider_button_delete"></div>
                <Button disabled={!hasSelected}
                        loading={loading} onClick={this.showModal}><Icon type="minus" theme="outlined"/>Eliminar</Button>
                <Modal
                    title="Eliminar Utilizador"
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancelar</Button>,
                        <Button key="submit" type="primary" loading={loadingDelete} onClick={this.handleOk}>
                            Eliminar
                        </Button>,
                    ]}>

                    <p>Tem a certeza que deseja eliminar o utilizador?</p>
                </Modal>


                <div className="layout_employee_table">

                    <Table rowKey={record => record.username} rowSelection={rowSelection} dataSource={dataSource}
                           columns={columns}
                           onRow={record => ({
                               onChange: () => {
                                   this.selectRow(record);
                               }
                           })}/>

                </div>
            </span>);
    }

}

export default UserTable;

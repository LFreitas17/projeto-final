import React, {Component} from 'react';
import {Table, Button, Modal, Icon} from 'antd';
import EditarEncomenda from './EditarEncomenda';
import {deleteOrder} from '../Requests.js';
import './TabelaEncomendas.css';
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

function compareByAlph(a, b) {
    if (a > b) {
        return -1;
    }
    if (a < b) {
        return 1;
    }
    return 0;
}

class TabelaEncomendas extends Component {
    state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        visible: false,
        loadingDelete: false,
        encomendasInfo: {}
    };

    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
                select: true,
            });
        }, 1000);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });

    }

    handleOk = () => {
        this.setState({loadingDelete: true});
        const encomendaId = this.state.selectedRowKeys[0];

        deleteOrder(encomendaId, this.props.onCreated);

        setTimeout(() => {
            this.setState({loadingDelete: false, visible: false, selectedRowKeys: []});
        }, 500);
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});

    }

    selectRow = record => {
        this.setState({
            encomendasInfo: record
        })
    }

    render() {
        const dataSource = this.props.data;
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const hasSelected = selectedRowKeys.length === 1;

        const columns = [
            {
                title: 'Nome',
                dataIndex: 'name',
                sorter: (a, b) => compareByAlph(a.name, b.name)
            }, {
                title: 'Data de Início',
                dataIndex: 'startDate',
                sorter: (a, b) => compareByAlph(a.startDate, b.startDate)
            }, {
                title: 'Data de Fim',
                dataIndex: 'endDate',
                sorter: (a, b) => compareByAlph(a.endDate, b.endDate)
            }, {
                title: 'Descrição',
                dataIndex: 'description',
                sorter: (a, b) => compareByAlph(a.description, b.description)
            },
            {
                title: 'Nome do Fornecedor',
                dataIndex: 'supplier.name',
                sorter: (a, b) => compareByAlph(a.supplier.name, b.supplier.name)
            }];

        const {visible, loadingDelete} = this.state;
        const encomendaId = this.state.selectedRowKeys[0];

        function exportSuppliers(data) {
            const allSupplier = [];
            for(let item of data) {
                if(item.supplier ==null){
                    allSupplier.push("");
                }else {
                    allSupplier.push(item.supplier)
                }
            }
            return allSupplier;
        }

        function exportCargoes(data) {
            const allCargoes = [];
            for(let item of data) {

                for(let it of item.cargoes) {
                    if (it == null) {
                        allCargoes.push("");
                    } else {
                        allCargoes.push(it)
                    }
                }
            }
            return allCargoes;
        }

        function exportBlocks(data) {

            const allCargoes2 = exportCargoes(data);

            const allBlocks = [];
            for(let item of allCargoes2) {
                for (let it of item.blocks) {
                    if (it == null) {
                        allBlocks.push("");
                    } else {
                        allBlocks.push(it)
                    }
                }
            }
            return allBlocks;
        }
        return (

            <span >
                <div className="divider_cargo_delete"></div>
                 <ExcelFile element={<Button><Icon type="vertical-align-bottom" theme="outlined"/>Exportar Encomendas</Button>}>
                <ExcelSheet data={this.props.data} name="Encomenda">
                    <ExcelColumn label="Name" value="name"/>
                    <ExcelColumn label="Data de Inicio" value="startDate"/>
                    <ExcelColumn label="Data de Final" value="endDate"/>
                    <ExcelColumn label="Descrição" value="description"/>
                </ExcelSheet>
                <ExcelSheet data={exportSuppliers(this.props.data)} name="Fornecedor">
                    <ExcelColumn label="Name" value="name"/>
                    <ExcelColumn label="Morada" value="address"/>
                    <ExcelColumn label="NIF" value="nif"/>
                    <ExcelColumn label="Telephone" value="telephone"/>
                </ExcelSheet>
                <ExcelSheet data={exportCargoes(this.props.data)} name="Cargas">
                    <ExcelColumn label="Número da Guia" value="numberGuide"/>
                    <ExcelColumn label="Data de Receção" value="receptionDate"/>
                    <ExcelColumn label="Preço" value="price"/>
                    <ExcelColumn label="Peso" value="weight"/>
                    <ExcelColumn label="Tipo de Compra" value="purchaseType"/>
                    <ExcelColumn label="Tipo de Carga" value="type"/>
                    <ExcelColumn label="Nome da Encomenda" value="nameOrder"/>
                </ExcelSheet>
                <ExcelSheet data={exportBlocks(this.props.data)} name="Blocos">
                    <ExcelColumn label="Número do Bloco" value="blockNumber"/>
                    <ExcelColumn label="Altura" value="height"/>
                    <ExcelColumn label="Comprimento" value="lenght"/>
                    <ExcelColumn label="Largura" value="width"/>
                    <ExcelColumn label="Qualidade do Bloco" value="quality"/>
                    <ExcelColumn label="Avaliação do Bloco" value="rating"/>
                    <ExcelColumn label="Descrição" value="description"/>
                    <ExcelColumn label="Número da Guia" value="numberGuide"/>
                </ExcelSheet>
            </ExcelFile>
                <div className="divider_cargo_delete"></div>
                    <EditarEncomenda data={this.state.encomendasInfo} onEdit={this.props.onCreated} disable={!hasSelected}
                               loading={loading} encomendaId={encomendaId}/>
                <div className="divider_cargo_delete"></div>
                <Button disabled={!hasSelected}
                        loading={loading} onClick={this.showModal}><Icon type="minus" theme="outlined"/>Apagar Encomenda</Button>
                <Modal
                    title="Apagar Encomenda"
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancelar</Button>,
                        <Button key="submit" type="primary" loading={loadingDelete} onClick={this.handleOk}>
                            Apagar
                        </Button>,
                    ]}>

                    <p>Are you sure you want to delete cargo and his allocation to blocks?</p>
                </Modal>


                <div className="layout_employee_table">

                    <Table rowKey={record => record.id} rowSelection={rowSelection} dataSource={dataSource}
                           columns={columns}
                           onRow={record => ({
                               onChange: () => {
                                   this.selectRow(record);
                               }
                           })}/>

                </div>
            </span>);
    }

}

export default TabelaEncomendas;

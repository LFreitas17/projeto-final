import React, { Component } from 'react';
import { getAllSuppliers } from '../Requests.js';
import 'antd/dist/antd.css';
import {Table, Button, Icon} from 'antd';
import CriarFornecedor from './CriarFornecedor.js';
import '../BlocksTable.css';

function compareByAlph(a, b) {
  if (a > b) {
      return -1;
  }
  if (a < b) {
      return 1;
  }
  return 0;
}

class ListaFornecedores extends Component {
  constructor() {
    super();
    this.state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        fornecedores: [],
        fornecedor: {}
    }

    this.saveList=this.saveList.bind(this);
  }
  
  componentDidMount() {
    getAllSuppliers(this.saveList);
  }

  saveList(response){
      this.setState({ fornecedores: response });
  }



start = () => {
    this.setState({loading: true});
    setTimeout(() => {
        this.setState({
            selectedRowKeys: [],
            loading: false,
            select: true,
        });
    }, 1000);
}


handleOk = () => {

    const fornecedorInfo = this.state.fornecedor;

    this.props.updateId(fornecedorInfo);


    this.props.onAdd();
}

onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({selectedRowKeys});

}

selectRow = record => {
    this.setState({
        fornecedor: record
    })
}

render() {
    const {loading, selectedRowKeys} = this.state;
    const listaFornecedores = this.state.fornecedores || [];
    const rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectChange,
    };

    const hasSelected = selectedRowKeys.length === 1;
    const createSupplier = selectedRowKeys.length === 0;

    const columns = [{
        title: 'Nome',
        dataIndex: 'name',
        sorter: (a, b) => compareByAlph(a.name, b.name)
    }, {
        title: 'Telefone',
        dataIndex: 'telephone',
        sorter: (a, b) => compareByAlph(a.telephone, b.telephone)
    }, {
        title: 'Morada',
        dataIndex: 'address',
        sorter: (a, b) => compareByAlph(a.address, b.address)
    }, {
      title: 'NIF',
      dataIndex: 'nif',
      sorter: (a, b) => compareByAlph(a.nif, b.nif)
  }];

    return (
        <span>
            <div className="divider_button_delete"></div>
                <CriarFornecedor onEdit={this.onCreated} disable={!createSupplier} created={this.saveList}
                           loading={loading} />
            <div className="divider_button_delete"></div>
            <Button disabled={!hasSelected}
                    loading={loading} onClick={this.handleOk} ><Icon type="edit" theme="outlined"/>Adicionar Fornecedor</Button>


            <div className="layout_employee_table">

                <Table rowKey={record => record.id} rowSelection={rowSelection} dataSource={listaFornecedores}
                       columns={columns}
                       onRow={record => ({
                           onChange: () => {
                               this.selectRow(record);
                           }
                       })}/>

            </div>
        </span>);
}

}
 
  
export default ListaFornecedores;

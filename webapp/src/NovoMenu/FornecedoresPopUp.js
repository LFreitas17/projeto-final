import React from 'react';
import { Modal, Button, Popover } from 'antd';
import ListaFornecedores from './ListaFornecedores.js';

class FornecedoresPopUp extends React.Component {
    state = {
        loading: false,
        visible: false,
        fornecedorId: ''
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({ loading: true });
        setTimeout(() => {
            this.setState({ loading: false, visible: false });
        }, 1000);
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    render() {
        const { visible } = this.state;

        return (
            <div>
                <Popover content="View Details">
                    <Button onClick={this.showModal}>
                        Selecionar Fornecedor
                    </Button>
                </Popover>
                <Modal
                    width="80%"
                    visible={visible}
                    title="Lista de Fornecedores"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancelar</Button>
                    ]}
                >
                      <ListaFornecedores onAdd={this.handleCancel} updateId={this.props.updateId}/>
                </Modal>
            </div>
        );
    }
}


export default FornecedoresPopUp;
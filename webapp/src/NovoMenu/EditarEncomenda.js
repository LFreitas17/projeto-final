import React from 'react';
import {Button, Modal, Form, Input, Icon} from 'antd';
import {updateOrder} from '../Requests';
import FornecedoresPopUp from "./FornecedoresPopUp";

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
            fornecedorInfo: {}
        }

        onChange = (e) => {
            console.log('radio checked', e.target.value);
            console.log('data', this.props.data);
            this.setState({
                value: e.target.value,
            });
        }

        render() {
            const {visible, onCancel, onCreate, form, updateId} = this.props;
            const {getFieldDecorator} = form;
            const encomandaInfo = this.props.data

            return (
                <Modal
                    visible={visible}
                    title="Editar Encomenda"
                    okText="Editar"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Nome">
                            {getFieldDecorator('name', {
                                initialValue: encomandaInfo.name,
                                rules: [{required: true, message: 'Por favor intorduzir o nome!'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Início">
                            {getFieldDecorator('startDate', {
                                initialValue: encomandaInfo.startDate,
                                rules: [{required: true, message: 'Por favor intorduzir a data de início!'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Fim">
                            {getFieldDecorator('endDate', {
                                initialValue: encomandaInfo.endDate,
                                rules: [{required: false, message: 'Por favor intorduzir a data de fim!'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Descrição">
                            {getFieldDecorator('description', {
                                initialValue: encomandaInfo.description,
                                rules: [{required: false, message: 'Por favor intorduzir a descrição!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Fornecedor">
                            {getFieldDecorator('startDate', {
                                rules: [{ required: true, message: 'Por favor associe um fornecedor!' }],
                            })(
                                <FornecedoresPopUp updateId={updateId}/>

                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class EditCargo extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        const encomendaId = this.props.encomendaId;
        const fornecedor = this.state.fornecedorInfo;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            updateOrder(values, encomendaId, fornecedor, this.props.onEdit);

            form.resetFields();
            this.setState({visible: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    addSupplier = (info) => {
        console.log("updateId", info);
        this.setState({fornecedorInfo: info});
    }
    render() {

        return (
            <span>
        <Button onClick={this.showModal} disabled={this.props.disable}
                loading={this.props.loading}><Icon type="edit" theme="outlined"/>Editar Encomenda</Button>
          <CollectionCreateForm data={this.props.data}
                                wrappedComponentRef={this.saveFormRef}
                                visible={this.state.visible}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
                                updateId={this.addSupplier}
          />
      </span>
        );
    }
}

export default EditCargo;
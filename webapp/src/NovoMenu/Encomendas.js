import React, { Component } from 'react';
import CollectionsPage from './AlterarEncomendas.js';
import { getAllOrders } from '../Requests.js';

class Encomendas extends Component {

    constructor() {
        super();
        this.state = {
            encomendas: [],
        }

        this.saveList=this.saveList.bind(this);
    }

    componentDidMount() {
        getAllOrders(this.saveList);
    }

    saveList(response){
        this.setState({ encomendas: response });
    }

    render() {
        const listaEncomendas = this.state.encomendas || [];
        return (
            <div >
                <CollectionsPage created={this.saveList}  encomendas={listaEncomendas}/>
            </div>
        );
    }
}
export default Encomendas;
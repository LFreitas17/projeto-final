import React from 'react';
import {Button, Modal, Form, Input, Icon} from 'antd';
import {addSupplier} from '../Requests';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
        }

        onChange = (e) => {

            this.setState({
                value: e.target.value,
            });
        }

        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;

            return (
                <Modal
                    visible={visible}
                    title="Criar Fornecedor"
                    okText="Adicionar"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Nome">
                            {getFieldDecorator('name', {
                               
                                rules:[{required: true, message: 'Por favor insira o nome!'}]
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="Telefone">
                            {getFieldDecorator('telephone', {
                           
                                rules: [{required: true, message: 'Por favor insira o telefone!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="Morada">
                            {getFieldDecorator('address', {  
                                rules: [{required: true, message: 'Por favor insira a morada!'}],
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="NIF">
                            {getFieldDecorator('nif', {
                                rules: [{required: true, message: 'Por favor insira o NF!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>

                    </Form>
                </Modal>
            );
        }
    }
);

class CriarFornecedor extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            addSupplier(values, this.props.created);

            form.resetFields();
            this.setState({visible: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {

        return (
            <span>
        <Button onClick={this.showModal} disabled={this.props.disable}
                loading={this.props.loading}><Icon type="edit" theme="outlined"/>Criar Fornecedor</Button>
          <CollectionCreateForm data={this.props.data}
                                wrappedComponentRef={this.saveFormRef}
                                visible={this.state.visible}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
          />
      </span>
        );
    }
}

export default CriarFornecedor;

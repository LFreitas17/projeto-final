import React from 'react';
import { Button, Modal, Form, Input, Icon } from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import TabelaEncomendas from './TabelaEncomendas.js';
import FornecedoresPopUp from './FornecedoresPopUp.js';
import {DatePicker} from 'antd';
import { createOrder } from '../Requests';
import './AlterarEncomendas.css';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form, updateId} = this.props;
            const { getFieldDecorator } = form;
            const dateFormat= 'DD/MM/YYYY';
            const fornecedor= updateId.name || [];
            return (
                <Modal
                    visible={visible}
                    title="Adicionar Encomenda"
                    okText="Adicionar"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Nome">
                            {getFieldDecorator('name', {
                                rules: [{ required: true, message: 'Por favor introduzir o nome!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Início">
                            {getFieldDecorator('startDate', {
                                rules: [{ required: true, message: 'Por favor introduzir a data de início!' }],
                            })(
                                <DatePicker format={dateFormat}/>
                            )}
                        </FormItem>
                        <FormItem label="Data de Fim">
                            {getFieldDecorator('endDate', {
                                rules: [{ required: false, message: 'Por favor introduzir a data de fim!' }],
                            })(
                                <DatePicker format={dateFormat}/>
                            )}
                        </FormItem>
                        <FormItem label="Descrição">
                            {getFieldDecorator('description', {
                                rules: [{ required: false, message: 'Por favor introduzir a descrição!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem label="Fornecedor">
                            {getFieldDecorator('supplier', {
                                rules: [{ required: true, message: 'Por favor crie ou associe um fornecedor!' }],
                            })(
                                <span>
                                Nome: {fornecedor} <FornecedoresPopUp updateId={updateId}/>
                                </span>
                                    
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CollectionsPage extends React.Component {
    state = {
        visible: false,
        fornecedorInfo: {}
    };

    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    addSupplier = (info) => {
        console.log("updateId", info);
        this.setState({fornecedorInfo: info});
    }

    handleCreate = () => {
        const form = this.formRef.props.form; 
        const fornecedor = this.state.fornecedorInfo;
        console.log("1" + fornecedor)

        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form1: ', values);
            console.log("2" +  fornecedor)
            createOrder(values, fornecedor,this.props.created);

            form.resetFields();
            this.setState({ visible: false });
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {
        return (
            <div>
                <ButtonGroup className= 'layout_cargo_buttons'>
                    <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined" />Adicionar Encomenda</Button>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                        updateId={this.addSupplier}
                    />
                    <TabelaEncomendas onCreated={this.props.created} data={this.props.encomendas} />

                </ButtonGroup>
            </div>
        );
    }
}

export default CollectionsPage;

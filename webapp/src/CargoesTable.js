import React, {Component} from 'react';
// import {CSVLink} from "react-csv";
import CsvDownloader from 'react-csv-downloader';
import ButtonGroup from 'antd/lib/button/button-group';

import {Button, Dropdown, Form, Icon, Input, Menu, Modal, Table} from 'antd';
import {addBlock2, getAllCargoes2} from "./Requests";
import './CargoesTable.css';

const FormItem = Form.Item;
const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;
            return (
                <Modal
                    visible={visible}
                    title="Add Block"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Height">
                            {getFieldDecorator('height', {
                                rules: [{required: false, message: 'Please input the block height!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Lenght">
                            {getFieldDecorator('length', {
                                rules: [{required: false, message: 'Please input the block lenght!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Width">
                            {getFieldDecorator('width', {
                                rules: [{required: false, message: 'Please input the block width!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Block Quality (dropdown)">
                            {getFieldDecorator('quality', {
                                rules: [{required: false, message: 'Please input the block quality!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Block Rating(dropdown)">
                            {getFieldDecorator('rating', {
                                rules: [{required: false, message: 'Please input the block rating!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Block Description">
                            {getFieldDecorator('description', {
                                rules: [{required: true, message: 'Please input the block description!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="graniteDTO.family">
                            {getFieldDecorator('graniteDTO.family', {
                                rules: [{required: true, message: 'Please input the block description!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="graniteDTO.type">
                            {getFieldDecorator('graniteDTO.type', {
                                rules: [{required: false, message: 'Please input the block description!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="graniteDTO.price">
                            {getFieldDecorator('graniteDTO.price', {
                                rules: [{required: true, message: 'Please input the block description!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="idCargo">
                            {getFieldDecorator('idCargo', {
                                rules: [{required: true, message: 'Please input the block description!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CargoesTable extends Component {

    constructor() {
        super();
        this.state = {
            cargoesList: [],
            blockList: []
        }
        this.saveList = this.saveList.bind(this);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addBlock2(values, this.props.onCreated);

            form.resetFields();
            this.setState({visible: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    componentDidMount() {
        getAllCargoes2(this.saveList);
    }

    saveList(response) {
        this.setState({cargoesList: response});
    }


    render() {

        const menu = (
            <Menu>
                <Menu.Item>Action 1</Menu.Item>
                <Menu.Item>Action 2</Menu.Item>
            </Menu>
        );

        function handleMenuClick(e) {
            console.log('click', e);
        }

        const menu2 = (
            <Menu onClick={handleMenuClick}>
                <Menu.Item key="1" onClick={this.showModal}>1st item</Menu.Item>
                <Menu.Item key="2">2nd item</Menu.Item>
                <Menu.Item key="3">3rd item</Menu.Item>
            </Menu>
        );

        const columns = [
            {title: 'Number Guide', dataIndex: 'numberGuide', key: 'numberGuide'},
            {title: 'Reception Date', dataIndex: 'receptionDate', key: 'receptionDate'},
            {title: 'Weight', dataIndex: 'weight', key: 'weight'},
            {title: 'Price', dataIndex: 'price', key: 'price'},
            {title: 'Type', dataIndex: 'type', key: 'type'},
            {title: 'Purchase Type', dataIndex: 'purchaseType', key: 'purchaseType'},
            {title: 'State', dataIndex: 'state', key: 'state'},
            {
                title: 'Action', key: 'operation', render: () =>
                    <Dropdown overlay={menu2}>
                        <Button>
                            Action <Icon type="down"/>
                        </Button>
                    </Dropdown>
            },
        ];

        const expandedRowRender = (cargo) => {
            const columns = [
                {title: 'Height', dataIndex: 'height', key: 'height'},
                {title: 'Length', dataIndex: 'length', key: 'length'},
                {title: 'Width', dataIndex: 'width', key: 'width'},
                {title: 'Quality', dataIndex: 'quality', key: 'quality'},
                {title: 'Rating', dataIndex: 'rating', key: 'rating'},
                {title: 'Description', dataIndex: 'description', key: 'description'},
                {
                    title: 'Action',
                    dataIndex: 'operation',
                    key: 'operation',
                    render: () => (
                        <span className="table-operation">
              <a>Pause</a>
              <a>Stop</a>
              <Dropdown overlay={menu}>
                <a>
                  More <Icon type="down"/>
                </a>
              </Dropdown>
            </span>
                    ),
                },
            ];

            const data = cargo.blocks || [];

            return <Table columns={columns} dataSource={data} pagination={false}/>;
        };

        const cargoList = this.state.cargoesList || [];
        const cargoList2 = cargoList;
        cargoList2.forEach(cargo => {
            cargo.blocks = JSON.stringify(cargo.blocks)
        });
        console.log(cargoList2);
        // const jsonObject = JSON.stringify(cargoList.toString());
        return (
            <div>
                <div>
                    <ButtonGroup className='layout_cargo_buttons'>
                        <Button type="primary" onClick={this.showModal}> <Icon type="plus"
                                                                               theme="outlined"/>Add</Button>
                        <CollectionCreateForm
                            wrappedComponentRef={this.saveFormRef}
                            visible={this.state.visible}
                            onCancel={this.handleCancel}
                            onCreate={this.handleCreate}
                        />

                        <CsvDownloader filename="myfile" separator=";" datas={cargoList2}>
                            <Button type="primary"> <Icon type="plus" theme="outlined"/> DOWNLOAD</Button>
                        </CsvDownloader>

                    </ButtonGroup>
                </div>

                <div className="layout_employee_table"></div>
                <CollectionCreateForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
                <Table
                    className="components-table-demo-nested"
                    columns={columns}
                    expandedRowRender={expandedRowRender}
                    dataSource={cargoList}
                />
            </div>
        );

    }
}

export default CargoesTable;


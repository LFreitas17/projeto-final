import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import Users from './Users';
import Block from './Block';
import Cargo from './Cargo';
import Encomendas from './NovoMenu/Encomendas.js'

import './App.css';
import "antd/dist/antd.css";
import { Layout, Menu, Icon } from 'antd';
import PosReception from "./PosRececao/PosReception";
import ValidarRececao from "./ValidarRececao/ValidarRececao";
import VerifyCargos from "./VerifyCargos"
import Granite from "./Granite";


const ReactFragment=React.Fragment;

class Gestao extends Component {
    render() {
      const { Content, Sider } = Layout;
      const { SubMenu } = Menu;
      return (
            <Layout style={{ padding: '24px 0', background: '#fff' }}>
              <Sider width={200} style={{ background: '#fff' }}>
                <Menu
                  mode="inline"
                  defaultSelectedKeys={['1']}
                  defaultOpenKeys={['sub1']}
                  style={{ height: '100%' }}
                >
                  <SubMenu
                    key="sub1"
                    title={
                      <span>
                        <Icon type="user" />
                        Gestão
                      </span>
                    }
                  >
                    <Menu.Item key="1">
                      <Link to="/gestao/users" >Utilizadores</Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                      <Link to="/gestao/encomendas">Encomendas</Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                      <Link to="/gestao/cargoes" >Cargas</Link>
                    </Menu.Item>
                    <Menu.Item key="9">
                      <Link to="/gestao/blocks" >Blocos</Link>
                    </Menu.Item>
                    <Menu.Item key="12">
                      <Link to="/gestao/granitos">Granitos</Link>
                    </Menu.Item>
                  </SubMenu>
                  <SubMenu
                    key="sub2"
                    title={
                      <span>
                        <Icon type="code-sandbox" style={{ fontSize: '16px'}}/>
                        Receção
                      </span>
                    }
                  >
                    <Menu.Item key="6">
                      <Link to="/gestao/posreception">Terminar Receção</Link>
                    </Menu.Item>
                    <Menu.Item key="7">
                      <Link to="/gestao/validar">Validar Receção</Link>
                    </Menu.Item>
                    <Menu.Item key="8">
                      <Link to="/gestao/verificada">Receções Verificadas</Link>
                    </Menu.Item>
                  </SubMenu>
                </Menu>
              </Sider>
              <Content style={{ padding: '0 24px', minHeight: 280 }}>
               <ReactFragment>
                <Route path="/gestao/users" component={Users}></Route>
                <Route path="/gestao/encomendas" component={ Encomendas }></Route>
                <Route path="/gestao/cargoes" component={Cargo}></Route>
                <Route path="/gestao/blocks" component={Block}></Route>
                <Route path="/gestao/granitos" component={ Granite }></Route>
                <Route path="/gestao/posreception" component={ PosReception }></Route>
                <Route path="/gestao/validar" component={ ValidarRececao }></Route>
                <Route path="/gestao/verificada" component={ VerifyCargos }></Route>

              </ReactFragment> 
              </Content>
            </Layout>
    );
}
}

export default Gestao;
import React, {Component} from 'react';
import {Table, Button, Modal, Icon} from 'antd';
import EditCargo from "./EditCargo";
import {deleteCargo} from './Requests.js';
import './CargoTable.css';
// import Moment from 'moment';


function compareByAlph(a, b) {
    if (a > b) {
        return -1;
    }
    if (a < b) {
        return 1;
    }
    return 0;
}

// Moment.locale('pt');
// const dt= Moment(receptionDate).format('d MMM');
class CargoTable extends Component {
    state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        visible: false,
        loadingDelete: false,
        cargoInfo: {}
    };

    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
                select: true,
            });
        }, 1000);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({loadingDelete: true});
        const cargoId = this.state.selectedRowKeys[0];

        deleteCargo(cargoId, this.props.onCreated);

        setTimeout(() => {
            this.setState({loadingDelete: false, visible: false, selectedRowKeys: []});
        }, 500);
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
       // this.setState({cargoInfo})

    }

    selectRow = record => {
        this.setState({
            cargoInfo: record
        })
        console.log("luis" + this.state.cargoInfo);
    }

    render() {
        const dataSource = this.props.data;
        console.log(dataSource)
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const hasSelected = selectedRowKeys.length === 1;
        const columns = [
            {
                title: 'Número da Guia',
                dataIndex: 'numberGuide',
                sorter: (a, b) => compareByAlph(a.numberGuide, b.numberGuide)
            },{
                title: 'Data de receção',
                dataIndex: 'receptionDate',
                sorter: (a, b) => compareByAlph(a.receptionDate, b.receptionDate)
            },{
                title: 'Peso (kg)',
                dataIndex: 'weight',
                sorter: (a, b) => compareByAlph(a.weight, b.weight)
            }, {
                title: 'Preço (€)',
                dataIndex: 'price',
                sorter: (a, b) => compareByAlph(a.price, b.price)
            }, {
                title: 'Tipo de Carga',
                dataIndex: 'type',
                sorter: (a, b) => compareByAlph(a.type, b.type)
            }, {
                title: 'Tipo de Compra',
                dataIndex: 'purchaseType',
                sorter: (a, b) => compareByAlph(a.purchaseType, b.purchaseType)
            }, {
                title: 'Estado da Encomenda',
                dataIndex: 'state',
                sorter: (a, b) => compareByAlph(a.state, b.state)
            }, {
                title: 'Nome Encomenda',
                dataIndex: 'nameOrder',
                sorter: (a, b) => compareByAlph(a.nameOrder, b.nameOrder)
            }, {
                title: '',
                dataIndex: "",
                key: "info",
                width: 10,
            }];

        const {visible, loadingDelete} = this.state;
        const idCargas = this.state.selectedRowKeys[0];
        console.log("id:" + idCargas);

        return (
            <span>
                <div className="divider_cargo_delete"></div>
                    <EditCargo data={this.state.cargoInfo} onEdit={this.props.onCreated} disable={!hasSelected}
                               loading={loading} cargoId={idCargas}/>
                <div className="divider_cargo_delete"></div>
                <Button disabled={!hasSelected}
                        loading={loading} onClick={this.showModal}><Icon type="minus" theme="outlined"/>Eliminar Carga</Button>
                <Modal
                    title="Delete Cargo"
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancelar</Button>,
                        <Button key="submit" type="primary" loading={loadingDelete} onClick={this.handleOk}>
                            Delete
                        </Button>,
                    ]}>
                    <p>Tem a certeza que quer apagar a carga?</p>
                </Modal>
                <div className="layout_employee_table">
                    <Table rowKey={record => record.id} rowSelection={rowSelection} dataSource={dataSource}
                           columns={columns}
                           onRow={record => ({
                               onChange: () => {
                                   this.selectRow(record);
                               }
                           })}
                    />
                </div>
            </span>);
    }

}

export default CargoTable;

import React from 'react';
import { Form, Select, InputNumber, Button } from 'antd';
import { getAllBlocks, addProjectToEmployee } from './Requests';


const Option = Select.Option;

const FormItem = Form.Item;

class EmployeeHorizontalForm extends React.Component {

    constructor() {
        super();
        this.state = {
            projects: [],
        }
        this.saveList=this.saveList.bind(this);
    }

    componentDidMount() {
        getAllBlocks(this.saveList);
    }

    saveList(response){
        this.setState({ projects: response })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.addProject(values.projectId, values.allocation);
            }
        });
    }

    addProject(projectId, allocation) {

        addProjectToEmployee(this.props.employeeId,allocation, projectId, this.props.created);
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <Form layout="inline" onSubmit={this.handleSubmit}>
                <FormItem
                >
                    {getFieldDecorator('projectId', {
                        rules: [{ required: true, message: '' }],
                    })(
                        <Select
                            showSearch
                            style={{ width: 200 }}
                            optionFilterProp="children"
                            onChange={this.handleChange}
                            onFocus={this.handleFocus}
                            onBlur={this.handleBlur}
                            filterOption={(input, option) =>
                                option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {this.state.projects.map(project => (
                                <Option key={project.projectId} value={project.projectId}>
                                    {project.name}
                                </Option>
                            ))}
                        </Select>
                    )}
                </FormItem>
                <FormItem
                >
                    {getFieldDecorator('allocation', {
                        rules: [{ required: true, message: '' }],
                    })(
                        <InputNumber min={1} max={100} size={200} />
                    )}
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                    >
                        Add
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

export default EmployeeHorizontalForm;

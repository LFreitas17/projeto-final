import React, { Component } from 'react';
import CollectionsPage from './CargoEdit.js';
import { getAllCargoes } from './Requests.js';

class Cargo extends Component {

    constructor() {
        super();
        this.state = {
            cargoes: [],
        }

        this.saveList=this.saveList.bind(this);
    }

    componentDidMount() {
        getAllCargoes(this.saveList);
    }

    saveList(response){
        this.setState({ cargoes: response });
    }

    render() {
        const cargoList = this.state.cargoes || [];
        return (
            <div >
                <CollectionsPage created={this.saveList}  cargoList={cargoList}/>
            </div>
        );
    }
}
export default Cargo;
import React from 'react';
import {Button, Modal, Form, Input, Icon, Select} from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import UserTable from './UserTable.js';
import './UserEdit.css';
import {createUser} from './Requests.js';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
        };

        onChange = (e) => {
            this.setState({
                value: e.target.value,
            });
        };

        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;

            return (
                <Modal
                    visible={visible}
                    title="Adicionar Utilizador"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Nome:">
                            {getFieldDecorator('username', {
                                rules: [{required: true, message: 'Por favor insira o nome do utilizador!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Palavra Passe:">
                            {getFieldDecorator('password', {
                                rules: [{required: true, message: 'Por favor insira a palavra passe do utilizador!'}],
                            })(
                                <Input type="password"/>
                            )}
                        </FormItem>
                        <FormItem label="Telémovel:">
                            {getFieldDecorator('telephone', {
                                rules: [{required: true, message: 'Por favor insira o telemóvel do utilizador!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Cargo:">
                            {getFieldDecorator('role.name', {
                                rules: [{required: true, message: 'Por favor insira o cargo do utilizador!'}],
                            })(
                                <Select style={{width: 120}}>
                                    <Option value="ADMIN">Administrador</Option>
                                    <Option value="SECRETARIO">Secretário</Option>
                                    <Option value="RECECIONISTA">Rececionista</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Morada:">
                            {getFieldDecorator('address', {
                                rules: [{required: true, message: 'Por favor insira a morada do utilizador!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CollectionsPage extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {
        this.setState({visible: false});
    };

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            //add new employee
            createUser(values, this.props.created);

            form.resetFields();
            this.setState({visible: false});
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    render() {
        return (
            <div>
                <ButtonGroup className='layout_user_buttons'>
                    <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined"/>Adicionar</Button>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                    />
                    <UserTable onChange={this.props.created} data={this.props.employeeList}/>

                </ButtonGroup>
            </div>
        );
    }
}

export default CollectionsPage;

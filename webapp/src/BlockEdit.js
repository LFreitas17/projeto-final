import React from 'react';
import {Button, Modal, Form, Input, InputNumber, Icon, Select} from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import BlocksTable from './BlocksTable.js'; 
import { addBlock } from './Requests';
import './BlockEdit.css';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
  class extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Adicionar Bloco"
          okText="Add"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="Número do Bloco">
              {getFieldDecorator('blockNumber', {
                rules: [{ required: true, message: 'Por favor insira o número do bloco!' }],
              })(
                  <InputNumber min={1} max={9999} size={200} />
              )}
            </FormItem>
            <FormItem label="Altura">
              {getFieldDecorator('height', {
                rules: [{ required: true, message: 'Por favor insira a altura do bloco!' }],
              })(
                  <InputNumber min={1} max={99999} size={200}/>
              )}
            </FormItem>
            <FormItem label="Comprimento">
              {getFieldDecorator('length', {
                rules: [{ required: true, message: 'Por favor insira o comprimento do bloco!' }],
              })(
                  <InputNumber min={1} max={99999} size={200}/>
              )}
            </FormItem>
            <FormItem label="Largura">
              {getFieldDecorator('width', {
                rules: [{ required: true, message: 'Por favor insira a largura do bloco!' }],
              })(
                  <InputNumber min={1} max={99999} size={200}/>
              )}
            </FormItem>
            <FormItem label="Qualidade do bloco">
              {getFieldDecorator('quality', {
                rules: [{ required: true, message: 'Por favor escolha a qualidade que está o bloco!' }],
              })(
                  <Select style={{ width: 120 }}>
                    <Option value="GOOD">BOM</Option>
                    <Option value="BAD">MAU</Option>
                  </Select>
              )}
            </FormItem>
            <FormItem label="Avaliação do bloco">
              {getFieldDecorator('rating', {
                rules: [{ required: true, message: 'Por favor escolha a avaliação devida para o bloco!' }],
              })(
                  <Select style={{ width: 120 }}>
                    <Option value="OK">OK</Option>
                    <Option value="NOT_OK">NOT_OK</Option>
                  </Select>
              )}
            </FormItem>
            <FormItem label="Descrição">
              {getFieldDecorator('description', {
                rules: [{ required: false, message: 'Por favor insira uma descrição!' }],
              })(
                  <Input />
              )}
            </FormItem>
            <FormItem label="Número da guia">
              {getFieldDecorator('numberOfGuide', {
                rules: [{ required: true, message: 'Por favor insira o número da guia da carga que este bloco esta associado!' }],
              })(
                  <Input />
              )}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
);

class CollectionsPage extends React.Component {
  state = {
    visible: false,
  };

  showModal = () => {
    this.setState({ visible: true });
  }

  handleCancel = () => {
    this.setState({ visible: false });
  }

  handleCreate = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      console.log('Received values of form: ', values);

      addBlock(values,this.props.created);

      form.resetFields();
      this.setState({ visible: false });
    });
  }

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  }

  render() {
    return (
        <div>
          <ButtonGroup className= 'layout_block_buttons'>
            <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined" />Adicionar Bloco</Button>
            <CollectionCreateForm
              wrappedComponentRef={this.saveFormRef}
              visible={this.state.visible}
              onCancel={this.handleCancel}
              onCreate={this.handleCreate}
            />  
            <BlocksTable onCreated={this.props.created} data={this.props.blockList} />
           
          </ButtonGroup>
        </div>
        );
      }
    }
    
    export default CollectionsPage;

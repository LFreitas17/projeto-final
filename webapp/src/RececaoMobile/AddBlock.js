import React from 'react';
import {message, Button, Form, Icon, Input, Modal, Select, InputNumber} from 'antd';
import {addBlock3} from '../Requests';
import '../BlockEdit.css';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, terminarRececao, form } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Adicionar Bloco"
                    footer={[
                        <Button key="cancelar" type="primary" onClick={onCancel}>Cancelar</Button>,
                        <Button key="terminar" type="primary" onClick={terminarRececao}>Terminar Receção</Button>,
                        <Button key="adicionar" type="primary" onClick={onCreate}><Icon type="plus" theme="outlined"/>Adicionar Outro Bloco</Button>,
                    ]}>
                    <Form layout="vertical">
                        <FormItem label="Número do Bloco">
                            {getFieldDecorator('blockNumber', {
                                rules: [{ required: true, message: 'Por favor insira o número do bloco!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Altura">
                            {getFieldDecorator('height', {
                                rules: [{ required: false, message: 'Por favor insira a altura do bloco!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Comprimento">
                            {getFieldDecorator('length', {
                                rules: [{ required: false, message: 'Por favor insira o comprimento do bloco!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Largura">
                            {getFieldDecorator('width', {
                                rules: [{ required: false, message: 'Por favor insira a largura do bloco!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Qualidade do bloco">
                            {getFieldDecorator('quality', {
                                rules: [{ required: false, message: 'Por favor escolha a qualidade que está o bloco!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="GOOD">BOM</Option>
                                    <Option value="BAD">MAU</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Avaliação do bloco">
                            {getFieldDecorator('rating', {
                                rules: [{ required: false, message: 'Por favor escolha a avaliação devida para o bloco!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="OK">OK</Option>
                                    <Option value="NOT_OK">NOT_OK</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Descrição">
                            {getFieldDecorator('description', {
                                rules: [{ required: false, message: 'Por favor insira uma descrição!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Número da guia">
                            {getFieldDecorator('numberOfGuide', {
                                rules: [{ required: false, message: 'Por favor insira o número da guia da carga que este bloco esta associado!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CollectionsPage extends React.Component {

    state = {
        visible: ''
    };

    // saveProps = (visibleBlock) =>{
    //     this.setState({ visible: visibleBlock });
    // }

    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
        this.props.visibleBlock(false);
    }

    handleCreate = () => {
        const infoAdd = () => {
            message.info('Adicionado com sucesso!');
        };
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addBlock3(values);

            infoAdd();

            form.resetFields();
            this.setState({ visible: false });
            this.props.visibleBlock(true);
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    finishReception = () => {
        const infoAdd = () => {
            message.info('Terminado com sucesso!');
        };
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addBlock3(values);

            infoAdd();

            form.resetFields();
            this.setState({ visible: true });
            this.props.visibleBlock(false);
        });
    }


    render() {
        //this.saveProps(this.props.visibleBlock);
        return (
            <div>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.props.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                        terminarRececao={this.finishReception}
                    />
            </div>
        );
    }
}

export default CollectionsPage;

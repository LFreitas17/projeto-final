import React from 'react';
import {Button, DatePicker, Form, Icon, Input, InputNumber, Modal, Select} from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import {addCargo2} from '../Requests';
import './ReceptionMobile.css';
import AddBlock from './AddBlock.js'
import SecondButton from "./SecondButton";

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            const dateFormat= 'DD/MM/YYYY';
            return (
                <Modal
                    visible={visible}
                    title="Adicionar Carga"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Número da Guia">
                            {getFieldDecorator('numberGuide', {
                                rules: [{ required: true, message: 'Por favor escreva o número da guia!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Receção">
                            {getFieldDecorator('receptionDate', {
                                rules: [{ required: false, message: 'Por favor insira a data de receção!' }],
                            })(
                                <DatePicker format={dateFormat}/>
                            )}
                        </FormItem>
                        <FormItem label="Peso">
                            {getFieldDecorator('weight', {
                                rules: [{ required: false, message: 'Por favor insira o peso da carga!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Preço">
                            {getFieldDecorator('price', {
                                rules: [{ required: false, message: 'Por favor insira o preço da carga!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Carga">
                            {getFieldDecorator('type', {
                                rules: [{ required: false, message: 'Por favor escolha o tipo da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="EUROS_TON">Por Tonelada</Option>
                                    <Option value="EUROS_MCUB">Por m³</Option>
                                    <Option value="EUROS_LOAD" >Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Compra">
                            {getFieldDecorator('purchaseType', {
                                rules: [{ required: false, message: 'Por favor escolha o tipo de compra!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="BY_BLOCK">Por Bloco</Option>
                                    <Option value="BY_LOAD">Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CollectionsPage extends React.Component {
    state = {
        visible: false,
        visibleBlock:false,
        visibleBlock2:false,
        visibleNumberGuide:false
    };

    changeBlockVisible = (vBlock) => {
        this.setState({visibleBlock: vBlock});
    }

    changeBlock2Visible = (vBlock) => {
        this.setState({visibleBlock2: vBlock});
    }

    changeNumberGuideVisible = (vNGuide) => {
        this.setState({visibleNumberGuide: vNGuide});
    }

    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addCargo2(values);

            form.resetFields();
            this.setState({ visible: false});
            this.changeBlockVisible(true);
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {
        return (
            <span>
                <ButtonGroup className= 'layout_mobile_buttons'>
                    <Button type="primary" onClick={this.showModal}> <Icon type="caret-right" theme="outlined" />Começar Receção</Button>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                    />
                </ButtonGroup>
                <div className="divider_button"></div>
                <ButtonGroup className= 'layout_mobile_buttons2'>
                    <SecondButton data={this.state.cargoInfo} visible={this.state.visibleNumberGuide} visibleBlock2={this.state.visibleBlock2} visibleNumberGuide={this.changeNumberGuideVisible} visibleBlockChange={this.changeBlock2Visible} />
                </ButtonGroup>
                <AddBlock visibleBlock={this.changeBlockVisible} visible={this.state.visibleBlock}/>
            </span>
        );
    }
}

export default CollectionsPage;
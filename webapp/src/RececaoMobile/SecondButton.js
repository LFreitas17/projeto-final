import React from 'react';
import {Button, Modal, Form, Input, Icon, message} from 'antd';
import '../CargoEdit.css';
import AssociarBloco from "./AssociarBloco";
import {existsNumberGuide, getAllCargoes3} from "../Requests";

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Adicionar Carga"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Número da Guia">
                            {getFieldDecorator('numberGuide', {
                                rules: [{ required: true, message: 'Por favor escreva o número da guia!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class SecondButton extends React.Component {

    constructor() {
        super();
        this.state = {
            visible: false,
            numberGuide: "",
            veracidade:""
        }

        this.saveList=this.saveList.bind(this);
    }

    showModal = () => {
        this.setState({ visible: true });
        this.props.visibleNumberGuide(true);
    }

    handleCancel = () => {
        this.setState({ visible: false });
        this.props.visibleNumberGuide(false);
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            this.numberGuideTrue(values);

            form.resetFields();
            this.setState({ visible: false });

        });
    }

    numberGuideTrue = (values) => {

       existsNumberGuide(values, this.saveList, this.coiso);

    };

    coiso = (values)  => {
        const infoAdd1 = () => {
            message.info('Não existe essa guia!');
        };

        const infoAdd2 = () => {
            message.info('Existe essa guia!');
        };
        console.log(values);
        // console.log("uiuiu", this.state.veracidade)

        if(this.state.veracidade==true){
            this.setState({numberGuide: values.numberGuide});
            this.props.visibleNumberGuide(false);
            this.props.visibleBlockChange(true);
            infoAdd2();
        }
        else {
            infoAdd1();
            this.props.visibleNumberGuide(true);
            this.props.visibleBlockChange(false);
        }
    }

    saveList(response){
        this.setState({ veracidade: response });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {
        return (
            <div>
                <Button type="primary" onClick={this.showModal}> <Icon type="branches" theme="outlined" />Associar Bloco a Carga</Button>
                <CollectionCreateForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.props.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}

                />
                <AssociarBloco visible={this.props.visibleBlock2} visibleBlockChange={this.props.visibleBlockChange} numberOfGuide={this.state.numberGuide}/>
            </div>
        );
    }
}

export default SecondButton;

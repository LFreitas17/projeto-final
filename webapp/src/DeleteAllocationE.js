import React, { Component } from 'react';
import { Icon, Popover } from 'antd';
import { deleteAllocationEmployee } from './Requests';

class DeleteAllocationEmployee extends Component {

    handleOk = () => {
        const employeeId = this.props.employeeId;
        const projectId=this.props.projectId;

        deleteAllocationEmployee(employeeId,projectId, this.props.onChange);

        setTimeout(() => {
            this.setState({ loadingDelete: false, visible: false });
        }, 1000);
    }

    render() {
        return (
            <div >
                <Popover content="Delete Allocation">
                <span onClick={this.handleOk}><Icon type="delete" theme="outlined" /></span>
                </Popover>
            </div>
        );
    }
}
export default DeleteAllocationEmployee;

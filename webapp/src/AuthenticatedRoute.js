import React, {Component} from 'react'
import {Route, Redirect} from 'react-router-dom'
import AuthenticationService from "./AuthenticationService";
import ReceptionMobile from "./RececaoMobile/ReceptionMobile";

class AuthenticatedRoute extends Component {
    render() {
        if (AuthenticationService.isUserLoggedIn()) {
            if (AuthenticationService.isUserAdmin()) {
                return <Route {...this.props} />
            } else {
                return <Route path="/rececao" component={ReceptionMobile}/>
            }
        } else {
            return <Redirect to="/login"/>
        }
    }
}

export default AuthenticatedRoute;
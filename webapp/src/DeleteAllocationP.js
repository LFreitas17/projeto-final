import React, { Component } from 'react';
import { Icon, Popover } from 'antd';
import { deleteAllocationProject } from './Requests';

class DeleteAllocationProject extends Component {

    handleOk = () => {
        const employeeId = this.props.employeeId;
        const projectId=this.props.projectId;

        deleteAllocationProject(employeeId,projectId, this.props.onChange);    
        setTimeout(() => {
            this.setState({ loadingDelete: false, visible: false });
        }, 1000);
    }

    render() {
        return (
            <div >
                <Popover content="Delete Allocation">
                <span onClick={this.handleOk}><Icon type="delete" theme="outlined" /></span>
                </Popover>
            </div>
        );
    }
}
export default DeleteAllocationProject;

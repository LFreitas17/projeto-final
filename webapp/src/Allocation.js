import React from 'react';
import { Modal, Button, Popover } from 'antd';
import EmployeeAllocation from './EmployeeAllocation.js';

class EmployeeProjects extends React.Component {
    state = {
        loading: false,
        visible: false,
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({ loading: true });
        setTimeout(() => {
            this.setState({ loading: false, visible: false });
        }, 1000);
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    render() {
        const { visible, loading } = this.state;

        return (
            <div>
                <Popover content="View Details">
                    <Button onClick={this.showModal}>
                        Luis
                    </Button>
                </Popover>
                <Modal
                    visible={visible}
                    title="Employees Project"
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>Save</Button>,
                    ]}
                >
                    <p>Employee:  {this.props.info.employeeName}</p>

                      <EmployeeAllocation employeeId={this.props.info.employeeId} /> 
                </Modal>
            </div>
        );
    }
}


export default EmployeeProjects;
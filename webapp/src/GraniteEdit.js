import React from 'react';
import {Button, Modal, Form, Input, Icon, InputNumber} from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import GranitesTable from './GranitesTable.js';
import { addGranite } from './Requests';
import './GraniteEdit.css';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Adicionar Granito"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Familia do granito">
                            {getFieldDecorator('family', {
                                rules: [{ required: true, message: 'Por favor insira a familia do granito!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Granito">
                            {getFieldDecorator('type', {
                                rules: [{ required: true, message: 'Por favor insira o tipo do granito!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Preço (€)">
                            {getFieldDecorator('price', {
                                rules: [{ required: true, message: 'Por favor insira o preço do granito!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CollectionsPage extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addGranite(values,this.props.created);

            form.resetFields();
            this.setState({ visible: false });
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {
        return (
            <div>
                <ButtonGroup className= 'layout_granite_buttons'>
                    <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined" />Adicionar Granito</Button>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                    />
                    <GranitesTable onCreated={this.props.created} data={this.props.graniteList} />

                </ButtonGroup>
            </div>
        );
    }
}

export default CollectionsPage;

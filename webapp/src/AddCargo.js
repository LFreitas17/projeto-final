import React from 'react';
import { Button, Modal, Form, Input, Icon } from 'antd';
import {DatePicker} from 'antd';
import { addCargo } from './Requests';
import './CargoEdit.css';

const FormItem = Form.Item;
const CollectionCreateForm1 = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            const dateFormat= 'DD/MM/YYYY';
            return (
                <Modal
                    visible={visible}
                    title="Add Cargo"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Number Guide">
                            {getFieldDecorator('numberGuide', {
                                rules: [{ required: true, message: 'Please input the number guide!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Reception Date">
                            {getFieldDecorator('receptionDate', {
                                rules: [{ required: false, message: 'Please input the cargo reception date!' }],
                            })(
                                <DatePicker format={dateFormat}/>
                            )}
                        </FormItem>
                        <FormItem label="Weight">
                            {getFieldDecorator('weight', {
                                rules: [{ required: true, message: 'Please input the weight!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Price">
                            {getFieldDecorator('price', {
                                rules: [{ required: true, message: 'Please input the price!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Type (dp)">
                            {getFieldDecorator('type', {
                                rules: [{ required: true, message: 'Please input the type!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Purchase Type (dp)">
                            {getFieldDecorator('purchaseType', {
                                rules: [{ required: true, message: 'Please input the purchase type!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="state (dp)">
                            {getFieldDecorator('state', {
                                rules: [{ required: true, message: 'Please input the state!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class AddCargo extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addCargo(values,this.props.created);

            form.resetFields();
            this.setState({ visible: false });
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }
    render() {
        return (
            <span>
            <div>
                <div className="divider_button_delete"></div>
                    <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined" />Começar CARGA</Button>
                    <CollectionCreateForm1
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                    />
            </div>
                </span>
        );
    }
}

export default AddCargo;
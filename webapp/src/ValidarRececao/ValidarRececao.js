import React, {Component} from 'react';
import {Form, Input, Modal, Select, Table, Divider, InputNumber} from 'antd';
import { getAllCargoes3, updateCargo2, endReception1, deleteBlock2, updateOrder3} from "../Requests";
import '../CargoesTable.css';
import EncomendasPopUp from './EncomendasPopUp';
import EditBlock from './EditBlock';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {

        state = {
            value: '',
            orderInfo: {}
        }

        onChange = (e) => {
            console.log('radio checked', e.target.value);
            this.setState({
                value: e.target.value,
            });
        }

        render() {
            const {visible, onCancel, onCreate, form, carga, updateId} = this.props;
            const {getFieldDecorator} = form;
           
            console.log("edit", carga);


            return (
                <Modal
                    visible={visible}
                    title="Editar Carga"
                    okText="Edit"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Número da Guia">
                            {getFieldDecorator('numberGuide', {
                                initialValue: carga.numberGuide,
                                rules: [{required: true, message: 'Por favor insira o número da guia!'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Receção">
                            {getFieldDecorator('receptionDate', {
                                initialValue: carga.receptionDate,
                                rules: [{required: false, message: 'Por favor insira a data de receção! DD/MM/YYYY'}],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Peso">
                            {getFieldDecorator('weight', {
                                initialValue: carga.weight,
                                rules: [{required: false, message: 'Por favor insira o peso da carga!'}],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Preço">
                            {getFieldDecorator('price', {
                                initialValue: carga.price,
                                rules: [{required: false, message: 'Por favor insira o preço da carga!'}],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Carga">
                            {getFieldDecorator('type', {
                                initialValue: carga.type,
                                rules: [{ required: false, message: 'Por favor escolha o tipo da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="EUROS_TON">Por Tonelada</Option>
                                    <Option value="EUROS_MCUB">Por m³</Option>
                                    <Option value="EUROS_LOAD" >Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Compra">
                            {getFieldDecorator('purchaseType', {
                                initialValue: carga.purchaseType,
                                rules: [{ required: false, message: 'Por favor escolha o tipo de compra!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="BY_BLOCK">Por Bloco</Option>
                                    <Option value="BY_LOAD">Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Estado da Carga">
                            {getFieldDecorator('state', {
                                initialValue: carga.state,
                                rules: [{ required: true, message: 'Por favor escolha o estado da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="NORMAL">Normal</Option>
                                    <Option value="NOT_FINISHED">Por Acabar</Option>
                                    <Option value="FINISHED">Concluida</Option>
                                    <Option value="VERIFIED">Verificada</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Encomenda">
                            {getFieldDecorator('order', 
                            {rules: [{ required: true, message: 'Por favor crie ou associe um fornecedor!' }],
                            })(
                                <span>
                                  <EncomendasPopUp updateId={updateId}/>
                                </span>
                                
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);


class PosReception extends Component {

    constructor(){
        super();
        this.state={
            cargoesList:[],
            blockList:[],
            cargoInfo:{},
            blockId:'',
            click: false
        }
        this.saveList = this.saveList.bind(this);
    }

    componentDidMount(){
        getAllCargoes3(this.saveList);
    }
    
    saveList(response) {

        this.setState({cargoesList: response});

    }

    onClick = () => {
        this.setState({click:true});

    }

    setCarga = (carga) => {
        console.log(carga);
        this.setState({cargoInfo: carga})
    }

    showModal = () => {
        this.setState({click: true});
    }

    handleCancel = () => {
        this.setState({click: false});
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        const carga = this.state.cargoInfo;
        const order = this.state.orderInfo;
        const supplier = order.supplier;
        const orderId = order.id;
        const cargoId = carga.id;

        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            updateCargo2(values, cargoId, this.saveList);
            updateOrder3(order, orderId, supplier, this.saveList);

            form.resetFields();
            this.setState({click: false});
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    endReception = () => {
        console.log("cargaaa", this.state.cargoInfo);
        const carga = this.state.cargoInfo;
        const cargoId =carga.id ;

        endReception1(cargoId, this.saveList);
    }

    deteleBlock= (render) => {

        const rende = render || ''
    
        const blockId = rende.id || '';

        deleteBlock2(blockId, this.saveList);
    }

    addOrder = (info) => {
        console.log("updateId", info);
        this.setState({orderInfo: info});
    }

    render() {
        
        const columns = [
            { title: 'Número da Guia', dataIndex: 'numberGuide', key: 'numberGuide' },
            { title: 'Data de receção', dataIndex: 'receptionDate', key: 'receptionDate' },
            { title: 'Peso (kg)', dataIndex: 'weight', key: 'weight' },
            { title: 'Preço (€)', dataIndex: 'price', key: 'price' },
            { title: 'Tipo de Carga', dataIndex: 'type', key: 'type' },
            { title: 'Tipo de Compra', dataIndex: 'purchaseType', key: 'purchaseType' },
            { title: 'Estado', dataIndex: 'state', key: 'state' },
            { title: '', key: 'operation', render: (text, render) =>
                    <span className="table-operation">
                        <Divider type="vertical" />
                        <a onClick= {this.endReception } onMouseOver={()=> this.setCarga(render)}>Validar Receção</a>
                        <Divider type="vertical" />
                        <a onClick={this.onClick} onMouseOver={()=> this.setCarga(render)}>Editar</a>
                        <Divider type="vertical" />
            </span> },
        ];

        const expandedRowRender = (cargo) => {
            const columns = [
                { title: '', dataIndex: '', key: '' },
                { title: 'Bloco', dataIndex: 'blockNumber', key: 'blockNumber' },
                { title: 'Altura (m)', dataIndex: 'height', key: 'height' },
                { title: 'Comprimento (m)', dataIndex: 'length', key: 'length' },
                { title: 'Largura (m)', dataIndex: 'width', key: 'width' },
                { title: 'Qualidade do bloco', dataIndex: 'quality', key: 'quality' },
                { title: 'Avaliação do bloco', dataIndex: 'rating', key: 'rating' },
                { title: 'Descrição', dataIndex: 'description', key: 'description' },
                { title: 'Número da guia', dataIndex: 'numberGuide', key: 'numberGuide' },
                {
                    title: '',
                    dataIndex: 'operation',
                    key: 'operation',
                    render: (text, render) => (

                    <span className="table-operation">
                        <EditBlock data={render} onEdit={this.saveList}/>
                        <Divider type="vertical" />  
                        <a onClick={() => this.deteleBlock(render)}>Eliminar</a>
                        <Divider type="vertical" />
                    </span>
                    ),
                },
            ];

            const data = cargo.blocks || [];
           
            return <Table columns={columns} dataSource={data} pagination={false} />;
        };

        const cargoList = this.state.cargoesList || [];

        return (
            <span>
        <div className="divider_button_delete">
            <Table
                className="components-table-demo-nested"
                columns={columns}
                expandedRowRender={expandedRowRender}
                dataSource={cargoList}
            />
            <CollectionCreateForm data={cargoList}
                                  wrappedComponentRef={this.saveFormRef}
                                  visible={this.state.click}
                                  onCancel={this.handleCancel}
                                  onCreate={this.handleCreate}
                                  carga={this.state.cargoInfo}
                                  updateId={this.addOrder}
            />
        </div>
    </span>
        );

    }
}
export default PosReception;
import React, { Component } from 'react';
import { getAllOrders } from '../Requests.js';
import 'antd/dist/antd.css';
import {Table, Button, Icon} from 'antd';
import '../BlocksTable.css';

function compareByAlph(a, b) {
  if (a > b) {
      return -1;
  }
  if (a < b) {
      return 1;
  }
  return 0;
}

class ListaEncomendas extends Component {
  constructor() {
    super();
    this.state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        encomendas: [],
        encomenda: {}
    }

    this.saveList=this.saveList.bind(this);
  }
  
  componentDidMount() {
    getAllOrders(this.saveList);
  }

  saveList(response){
      this.setState({ encomendas: response });
  }



start = () => {
    this.setState({loading: true});
    setTimeout(() => {
        this.setState({
            selectedRowKeys: [],
            loading: false,
            select: true,
        });
    }, 1000);
}


handleOk = () => {

    const encomendaInfo = this.state.encomenda;
    console.log("agora", encomendaInfo)
    this.props.updateId(encomendaInfo);

    this.props.onAdd();
}

onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({selectedRowKeys});

}

selectRow = record => {
    this.setState({
        encomenda: record
    })
}

render() {
    const {loading, selectedRowKeys} = this.state;
    const listaEncomendas = this.state.encomendas || [];
    const rowSelection = {
        selectedRowKeys,
        onChange: this.onSelectChange,
    };

    const hasSelected = selectedRowKeys.length === 1;

    const columns = [{
            title: 'Nome',
            dataIndex: 'name',
            sorter: (a, b) => compareByAlph(a.name, b.name)
        }, {
            title: 'Data de Início',
            dataIndex: 'startDate',
            sorter: (a, b) => compareByAlph(a.startDate, b.startDate)
        }, {
            title: 'Data de Fim',
            dataIndex: 'endDate',
            sorter: (a, b) => compareByAlph(a.endDate, b.endDate)
        }, {
            title: 'Descrição',
            dataIndex: 'description',
            sorter: (a, b) => compareByAlph(a.description, b.description)
        },
        {
            title: 'Nome do Fornecedor',
            dataIndex: 'supplier.name',
            sorter: (a, b) => compareByAlph(a.supplier.name, b.supplier.name)
        }];

    return (
        <span>
            <div className="divider_button_delete"></div>
            <Button disabled={!hasSelected}
                    loading={loading} onClick={this.handleOk} ><Icon type="edit" theme="outlined"/>Associar Encomenda</Button>

            <div className="layout_employee_table">

                <Table rowKey={record => record.id} rowSelection={rowSelection} dataSource={listaEncomendas}
                       columns={columns}
                       onRow={record => ({
                           onChange: () => {
                               this.selectRow(record);
                           }
                       })}/>

            </div>
        </span>);
}

}
 
  
export default ListaEncomendas;

import React, {Component} from 'react';
import {Table, Button, Modal, Icon} from 'antd';
import EditBlock from './EditBlock.js';
import {deleteBlock} from './Requests.js';
import './BlocksTable.css';

function compareByAlph(a, b) {
    if (a > b) {
        return -1;
    }
    if (a < b) {
        return 1;
    }
    return 0;
}

class BlocksTable extends Component {
    state = {
        selectedRowKeys: [],
        loading: false,
        select: true,
        visible: false,
        loadingDelete: false,
        blockInfo: {}
    };

    start = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
                select: true,
            });
        }, 1000);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({loadingDelete: true});
        const blockId = this.state.selectedRowKeys[0];

        deleteBlock(blockId, this.props.onCreated);

        setTimeout(() => {
            this.setState({loadingDelete: false, visible: false, selectedRowKeys: []});
        }, 500);
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});

    }

    selectRow = record => {
        this.setState({
            blockInfo: record
        })
    }

    render() {
        const dataSource = this.props.data;
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const hasSelected = selectedRowKeys.length === 1;

        const columns = [{
            title: 'Número do Bloco',
            dataIndex: 'blockNumber',
            sorter: (a, b) => compareByAlph(a.blockNumber, b.blockNumber)
        }, {
            title: 'Altura (m)',
            dataIndex: 'height',
            sorter: (a, b) => compareByAlph(a.height, b.height)
        }, {
            title: 'Comprimento (m)',
            dataIndex: 'length',
            sorter: (a, b) => compareByAlph(a.length, b.length)
        }, {
            title: 'Largura (m)',
            dataIndex: 'width',
            sorter: (a, b) => compareByAlph(a.width, b.width)
        }, {
            title: 'Qualidade do bloco',
            dataIndex: 'quality',
            sorter: (a, b) => compareByAlph(a.quality, b.quality)
        }, {
            title: 'Avaliação do bloco',
            dataIndex: 'rating',
            sorter: (a, b) => compareByAlph(a.rating, b.rating)
        }, {
            title: 'Descrição',
            dataIndex: 'description',
            sorter: (a, b) => compareByAlph(a.description, b.description)
        }, {
            title: 'Número da guia',
            dataIndex: 'numberOfGuide',
            sorter: (a, b) => compareByAlph(a.numberOfGuide, b.numberOfGuide)
        }];

        const {visible, loadingDelete} = this.state;
        const blockId = this.state.selectedRowKeys[0];

        return (
            <span>
                <div className="divider_button_delete"></div>
                    <EditBlock data={this.state.blockInfo} onEdit={this.props.onCreated} disable={!hasSelected}
                               loading={loading} blockId={blockId}/>
                <div className="divider_button_delete"></div>
                <Button disabled={!hasSelected}
                        loading={loading} onClick={this.showModal}><Icon type="minus" theme="outlined"/>Eliminar Bloco</Button>
                <Modal
                    title="Delete Block"
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={loadingDelete} onClick={this.handleOk}>
                            Delete
                        </Button>,
                    ]}>

                    <p>Tem a certeza que quer apagar o bloco?</p>
                </Modal>


                <div className="layout_employee_table">

                    <Table rowKey={record => record.id} rowSelection={rowSelection} dataSource={dataSource}
                           columns={columns}
                           onRow={record => ({
                               onChange: () => {
                                   this.selectRow(record);
                               }
                           })}/>

                </div>
            </span>);
    }

}

export default BlocksTable;

import React, {Component} from 'react';
import 'antd/dist/antd.css';
import './Login.css';
import {Form, Icon, Input, Button} from 'antd';
import AuthenticationService from './AuthenticationService';

class LoginComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]: event.target.value
            }
        )
    };

    loginClicked(event) {
        event.preventDefault();
        AuthenticationService.executeJwtAuthenticationService(this.state.username, this.state.password)
            .then((response) => {
                AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data);
                if (response.data.role === 'RECECIONISTA') {
                    this.props.history.push('/rececao')
                } else {
                    this.props.history.push('/gestao/encomendas')
                }
            }).catch(() => {
            this.setState({hasLoginFailed: true})
        })
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.loginClicked} className="login-form">
                <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{required: true, message: 'Por favor insira o utilizador!'}],
                    })(
                        <Input
                            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            name="username"
                            placeholder="Utilizador"
                            setFieldsValue={this.state.username}
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{required: true, message: 'Por favor insira a senha!'}],
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            name="password"
                            type="password"
                            placeholder="Palavra Passe"
                            setFieldsValue={this.state.password}
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Login
                    </Button>
                </Form.Item>
                {this.state.hasLoginFailed && <div className="alert alert-danger">Credenciais inválidas!</div>}
            </Form>
        );
    }
}

const Login = Form.create({name: 'normal_login'})(LoginComponent);

export default Login;

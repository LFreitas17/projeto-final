import React, { Component } from 'react';
import CollectionsPage from './UserEdit.js';
import { getAllUsers } from './Requests.js';

class Users extends Component {
    constructor(){
        super();
        this.state={
            employees:[],   
        }
        this.saveList = this.saveList.bind(this);
    }

componentDidMount(){
   getAllUsers(this.saveList);
}

saveList(response) {
    this.setState({employees: response});
}

    render() {
        const employeeList=this.state.employees || [];      
        return (
        <div >
        <CollectionsPage created={this.saveList}  employeeList={employeeList}/>
        </div>
        );
    }
}
export default Users;
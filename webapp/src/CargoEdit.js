import React from 'react';
import {Button, Modal, Form, Input, Icon, Select, InputNumber} from 'antd';
import ButtonGroup from 'antd/lib/button/button-group';
import CargoTable from './CargoTable.js';
import {DatePicker} from 'antd';
import { addCargo } from './Requests';
import './CargoEdit.css';

const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            const dateFormat= 'DD/MM/YYYY';
            return (
                <Modal
                    visible={visible}
                    title="Adicionar Carga"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Número da Guia">
                            {getFieldDecorator('numberGuide', {
                                rules: [{ required: true, message: 'Por favor escreva o número da guia!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Data de Receção">
                            {getFieldDecorator('receptionDate', {
                                rules: [{ required: false, message: 'Por favor insira a data de receção!' }],
                            })(
                                <DatePicker format={dateFormat}/>
                            )}
                        </FormItem>
                        <FormItem label="Peso">
                            {getFieldDecorator('weight', {
                                rules: [{ required: false, message: 'Por favor insira o peso da carga!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Preço">
                            {getFieldDecorator('price', {
                                rules: [{ required: false, message: 'Por favor insira o preço da carga!' }],
                            })(
                                <InputNumber min={1} max={99999} size={200}/>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Carga">
                            {getFieldDecorator('type', {
                                rules: [{ required: true, message: 'Por favor escolha o tipo da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="EUROS_TON">Por Tonelada</Option>
                                    <Option value="EUROS_MCUB">Por m³</Option>
                                    <Option value="EUROS_LOAD" >Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Tipo de Compra">
                            {getFieldDecorator('purchaseType', {
                                rules: [{ required: true, message: 'Por favor escolha o tipo de compra!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="BY_BLOCK">Por Bloco</Option>
                                    <Option value="BY_LOAD">Por Carga</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Estado da Encomenda">
                            {getFieldDecorator('state', {
                                rules: [{ required: true, message: 'Por favor escolha o estado da carga!' }],
                            })(
                                <Select style={{ width: 120 }}>
                                    <Option value="NORMAL">Normal</Option>
                                    <Option value="NOT_FINISHED">Por Acabar</Option>
                                    <Option value="FINISHED">Concluida</Option>
                                    <Option value="VERIFIED">Verificada</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Nome da Encomenda">
                            {getFieldDecorator('nameOrder', {
                                rules: [{ required: false, message: 'Por favor escolha a que encomenda pertence a carga!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class CollectionsPage extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addCargo(values,this.props.created);

            form.resetFields();
            this.setState({ visible: false });
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    render() {
        return (
            <div>
                <ButtonGroup className= 'layout_cargo_buttons'>
                    <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined" />Adicionar Carga</Button>
                    <CollectionCreateForm
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
                    />
                    <CargoTable onCreated={this.props.created} data={this.props.cargoList} />
                </ButtonGroup>
            </div>
        );
    }
}

export default CollectionsPage;

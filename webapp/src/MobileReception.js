import React, {Component} from 'react';
import {Button, Icon, Form, Modal, Input} from 'antd';
import {addBlock2, getAllCargoes2} from "./Requests";
import EditCargo from "./CargoTable";
import AddCargo from "./AddCargo";
import CollectionsPage from "./Cargo";

const FormItem = Form.Item;
const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Add Block"
                    okText="Add"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <FormItem label="Height">
                            {getFieldDecorator('height', {
                                rules: [{ required: false, message: 'Please input the block height!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Lenght">
                            {getFieldDecorator('length', {
                                rules: [{ required: false, message: 'Please input the block lenght!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Width">
                            {getFieldDecorator('width', {
                                rules: [{ required: false, message: 'Please input the block width!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Block Quality (dropdown)">
                            {getFieldDecorator('quality', {
                                rules: [{ required: false, message: 'Please input the block quality!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Block Rating(dropdown)">
                            {getFieldDecorator('rating', {
                                rules: [{ required: false, message: 'Please input the block rating!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="Block Description">
                            {getFieldDecorator('description', {
                                rules: [{ required: true, message: 'Please input the block description!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="graniteDTO.family">
                            {getFieldDecorator('graniteDTO.family', {
                                rules: [{ required: true, message: 'Please input the block description!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="graniteDTO.type">
                            {getFieldDecorator('graniteDTO.type', {
                                rules: [{ required: false, message: 'Please input the block description!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="graniteDTO.price">
                            {getFieldDecorator('graniteDTO.price', {
                                rules: [{ required: true, message: 'Please input the block description!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem label="idCargo">
                            {getFieldDecorator('idCargo', {
                                rules: [{ required: true, message: 'Please input the block description!' }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);


class MobileReception extends Component {

    constructor(){
        super();
        this.state={
            cargoesList:[],
            blockList:[]
        }
        this.saveList = this.saveList.bind(this);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            addBlock2(values,this.props.onCreated);

            form.resetFields();
            this.setState({ visible: true });
        });
    }

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    componentDidMount(){
        getAllCargoes2(this.saveList);
    }

    saveList(response) {
        this.setState({cargoesList: response});
    }

    render() {


        return (
            <span>
                <div>
                <div className="divider_button_delete"></div>
                <AddCargo created={this.saveList}/>
                <div className="divider_button_delete"></div>
                <Button type="primary" onClick={this.showModal}> <Icon type="plus" theme="outlined" />Adicionar Novo Bloco</Button>
                <CollectionCreateForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
        </div>
   </span>
        );

    }
}
export default MobileReception;


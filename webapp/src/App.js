import React, {Component} from 'react';
import {Route, Link, Switch} from 'react-router-dom';
import ReceptionMobile from './RececaoMobile/ReceptionMobile'
import './App.css';
import "antd/dist/antd.css";
import {Button, Layout, Menu} from 'antd';
import Management from "./Management";
import gra2003 from './gra_azul1.png';
import  Login  from './Login';
import AuthenticatedRoute from "./AuthenticatedRoute";
import AuthenticationService from "./AuthenticationService";

const ReactFragment = React.Fragment;

class App extends Component {
    render() {
        const {Header, Footer, Content} = Layout;
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();


        return (
            <div>
                <Layout>
                    <Header className="title_box" style={{background: "#e3e4ed", padding: "0 20px"}}>
                        <img src={gra2003} alt="Gra2003"></img>
                        {isUserLoggedIn && <Button type="primary" key="logout" style={{float: 'right'}} onClick={AuthenticationService.logout}>
                            <Link to="/login">Logout</Link></Button>}
                    </Header>

                    <Menu mode="horizontal" style={{background: "#e3e4ed"}}>
                        <Menu.Item key="gestao">
                            <Link to="/gestao">Gestão</Link>
                        </Menu.Item>
                        <div className="divider_button">|</div>
                        <Menu.Item key="reception">
                            <Link to="/rececao">Receção</Link>
                        </Menu.Item>
                        {/*<div className="divider_button">|</div>*/}
                        {/*{isUserLoggedIn && <Menu.Item key="logout" onClick={AuthenticationService.logout}>*/}
                            {/*<Link to="/login">Logout</Link>*/}
                        {/*</Menu.Item>}*/}
                    </Menu>

                    <Content style={{padding: '0 50px'}}>
                        <ReactFragment>
                            <Switch>
                                <Route path="/" exact component={Login}/>
                                <Route path="/login" exact component={Login}/>
                                <AuthenticatedRoute path="/rececao" component={ReceptionMobile}/>
                                <AuthenticatedRoute path="/gestao" component={Management}/>
                            </Switch>
                        </ReactFragment>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>GRA 2003 ©2019 Created by Freitas</Footer>
                </Layout>
            </div>
        );
    }
}

export default App;

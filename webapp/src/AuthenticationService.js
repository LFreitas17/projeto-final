import axios from 'axios'

const API_URL = 'http://localhost:8080/api/v1/token';

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
export const USER_TOKEN = 'userToken';
export const USER_ROLE = "userRole";

class AuthenticationService {
    executeJwtAuthenticationService(username, password) {
        return axios.post(`${API_URL}/authenticate`, {
            username,
            password
        })
    }

    registerSuccessfulLoginForJwt(username, responseData) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        sessionStorage.setItem(USER_ROLE, responseData.role);
        this.createJWTToken(responseData.token);
        this.setupAxiosInterceptors();
    }

    createJWTToken(token) {
        let bearerToken = 'Bearer ' + token;
        sessionStorage.setItem(USER_TOKEN, bearerToken);
        return bearerToken;
    }

    logout() {
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        sessionStorage.removeItem(USER_TOKEN);
        sessionStorage.removeItem(USER_ROLE);
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        return user !== null;
    }

    isUserAdmin() {
        let role = sessionStorage.getItem(USER_ROLE);
        return role === 'ADMIN' || role === 'SECRETARIO';
    }

    setupAxiosInterceptors() {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = sessionStorage.getItem(USER_TOKEN);
                }
                return config;
            }
        )
    }
}

export default new AuthenticationService()
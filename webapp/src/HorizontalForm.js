import React from 'react';
import { Form, Select, InputNumber, Button } from 'antd';
import {getAllUsers, addEmployeeToProject} from './Requests.js';

const Option = Select.Option;

const FormItem = Form.Item;

class HorizontalForm extends React.Component {

    constructor() {
        super();
        this.state = {
            employees: [],
        }
        this.saveList=this.saveList.bind(this);
    }

    componentDidMount() {
        getAllUsers(this.saveList);
    }

    saveList(response){
       this.setState({ employees: response });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
               const projectId = this.props.projectId;
                this.addEmployee(values.employeeId, values.allocation, projectId );
            }
        });
    }

    addEmployee(employeeId, allocation,projectId) {

        addEmployeeToProject(employeeId, allocation, projectId, this.props.created);
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <Form layout="inline" onSubmit={this.handleSubmit}>
                <FormItem
                >
                    {getFieldDecorator('employeeId', {
                        rules: [{ required: true, message: '' }],
                    })(
                        <Select
                            showSearch
                            style={{ width: 200 }}
                            optionFilterProp="children"
                            onChange={this.handleChange}
                            onFocus={this.handleFocus}
                            onBlur={this.handleBlur}
                            filterOption={(input, option) =>
                                option.props.children[0].toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {this.state.employees.map(employee => (
                                <Option key={employee.employeeId} value={employee.employeeId}>
                                    {employee.name}
                                </Option>
                            ))}
                        </Select>
                    )}
                </FormItem>
                <FormItem
                >
                    {getFieldDecorator('allocation', {
                        rules: [{ required: true, message: '' }],
                    })(
                        <InputNumber min={1} max={100} size={200} />
                    )}
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                    >
                        Add
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

export default HorizontalForm;

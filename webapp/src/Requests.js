import {USER_TOKEN} from "./AuthenticationService";

export function existsNumberGuide(values, callback1, callback2) {
    fetch(`http://localhost:8080/api/v1/cargoes/numberGuide/${values.numberGuide}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback1(parsedJson);
        })
        .then(asd => {
            callback2(values);
        })
}

//Cargo requests

export function getAllCargoes(callback) {
    fetch('http://localhost:8080/api/v1/cargoes', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function getAllCargoes2(callback) {
    fetch('http://localhost:8080/api/v1/cargoes/state/NOT_FINISHED', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function getAllCargoes3(callback) {
    fetch('http://localhost:8080/api/v1/cargoes/state/FINISHED', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function getAllCargoes4(callback) {
    fetch('http://localhost:8080/api/v1/cargoes/state/VERIFIED', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function addCargo(values, callback) {

    fetch('http://localhost:8080/api/v1/cargoes', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'receptionDate': values.receptionDate,
            'numberGuide': values.numberGuide,
            'weight': values.weight,
            'price': values.price,
            'type': values.type,
            'purchaseType': values.purchaseType,
            'state': values.state,
            'nameOrder': values.nameOrder
        })
    })
        .then(response => {
            return ((response.status === 201) ? getAllCargoes(callback) : getAllCargoes(callback))
        });
}

export function addCargo2(values) {

    fetch('http://localhost:8080/api/v1/cargoes', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'receptionDate': values.receptionDate,
            'numberGuide': values.numberGuide,
            'weight': values.weight,
            'price': values.price,
            'type': values.type,
            'purchaseType': values.purchaseType,
            'state': values.state,
            'nameOrder': values.nameOrder

        })
    })
        .then(response => {
            return ((response.status === 201) ? true : true)
        });
}

export function deleteCargo(cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/cargoes/${cargoId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes(callback) : getAllCargoes(callback))
    });

}


export function updateCargo(values, cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/cargoes/${cargoId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'receptionDate': values.receptionDate,
            'numberGuide': values.numberGuide,
            'weight': values.weight,
            'price': values.price,
            'type': values.type,
            'purchaseType': values.purchaseType,
            'state': values.state,
            'nameOrder': values.nameOrder
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes(callback) : alert('Can not change cargo'))
    });
}

export function updateCargo1(values, cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/cargoes/${cargoId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'receptionDate': values.receptionDate,
            'numberGuide': values.numberGuide,
            'weight': values.weight,
            'price': values.price,
            'type': values.type,
            'purchaseType': values.purchaseType,
            'state': values.state,
            'nameOrder': values.nameOrder
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes2(callback) : alert('Can not change cargo'))
    });
}

export function updateCargo2(values, cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/cargoes/${cargoId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'receptionDate': values.receptionDate,
            'numberGuide': values.numberGuide,
            'weight': values.weight,
            'price': values.price,
            'type': values.type,
            'purchaseType': values.purchaseType,
            'state': values.state,
            'nameOrder': values.nameOrder
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes3(callback) : alert('Can not change cargo'))
    });
}

export function getAllBlocksFromCargo(cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/block/${cargoId}`, {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}


//Block requests

export function getAllBlocks(callback) {
    fetch('http://localhost:8080/api/v1/blocks', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function addBlock(values, callback) {

    fetch('http://localhost:8080/api/v1/blocks', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'blockNumber': values.blockNumber,
            'quality': values.quality,
            'rating': values.rating,
            'description': values.description,
            'height': values.height,
            'length': values.length,
            'width': values.width,
            'numberOfGuide': values.numberOfGuide
        })
    })
        .then(response => {
            return ((response.status === 201) ? getAllBlocks(callback) : getAllBlocks(callback))
        });
}

export function addBlock2(values, callback) {

    fetch('http://localhost:8080/api/v1/blocks', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'quality': values.quality,
            'rating': values.rating,
            'description': values.description,
            'height': values.height,
            'length': values.length,
            'width': values.width,
            'graniteDTO': {
                'family': values.family,
                'type': values.type,
                'price': values.price
            },
            'numberOfGuide': values.numberOfGuide
        })
    })
        .then(response => {
            return ((response.status === 201) ? getAllCargoes2(callback) : getAllCargoes2(callback))
        });
}

export function addBlock3(values) {

    fetch('http://localhost:8080/api/v1/blocks', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'blockNumber': values.blockNumber,
            'quality': values.quality,
            'rating': values.rating,
            'description': values.description,
            'height': values.height,
            'length': values.length,
            'width': values.width,
            'numberOfGuide': values.numberOfGuide
        })
    })
        .then(response => {
            return ((response.status === 201) ? true : true)
        });
}

export function deleteBlock(blockId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/${blockId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        return ((response.status === 200) ? getAllBlocks(callback) : getAllBlocks(callback))
    });

}

export function deleteBlock1(blockId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/${blockId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes2(callback) : getAllCargoes2(callback))
    });

}

export function deleteBlock2(blockId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/${blockId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes3(callback) : getAllCargoes3(callback))
    });

}

export function updateBlock(values, blockId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/${blockId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'quality': values.quality,
            'rating': values.rating,
            'description': values.description,
            'height': values.height,
            'length': values.length,
            'width': values.width,
            'numberOfGuide': values.numberOfGuide
        })
    }).then(response => {
        return ((response.status === 200) ? getAllBlocks(callback) : alert('Can not change block'))
    });
}

export function updateBlock1(values, blockId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/${blockId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'quality': values.quality,
            'rating': values.rating,
            'description': values.description,
            'height': values.height,
            'length': values.length,
            'width': values.width,
            'numberOfGuide': values.numberOfGuide
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes2(callback) : alert('Can not change block'))
    });
}

export function updateBlock2(values, blockId, callback) {
    fetch(`http://localhost:8080/api/v1/blocks/${blockId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'height': values.height,
            'length': values.length,
            'width': values.width,
            'quality': values.quality,
            'rating': values.rating,
            'description': values.description,
            'graniteDTO': {
                'family': values.family,
                'type': values.type,
                'price': values.price
            },
            'numberOfGuide': values.numberOfGuide
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes3(callback) : alert('Can not change block'))
    });
}

//Encomendas
export function getAllOrders(callback) {
    fetch('http://localhost:8080/api/v1/orders', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function createOrder(values, fornecedor, callback) {
    fetch('http://localhost:8080/api/v1/orders', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'name': values.name,
            'startDate': values.startDate,
            'endDate': values.endDate,
            'description': values.description,
            'supplier': {
                'id': fornecedor.id,
                'name': fornecedor.name,
                'telephone': fornecedor.telephone,
                'address': fornecedor.address,
                'nif': fornecedor.nif,
            }
        })
    }).then(response => {
        return ((response.status === 201) ? getAllOrders(callback) : getAllOrders(callback))
    });
}

export function deleteOrder(encomendaId, callback) {
    fetch(`http://localhost:8080/api/v1/orders/${encomendaId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        if (response.status === 200) {
            return (getAllOrders(callback))
        }
    });

}

export function updateOrder(values, encomendaId, fornecedor, callback) {
    fetch(`http://localhost:8080/api/v1/orders/${encomendaId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'name': values.name,
            'startDate': values.startDate,
            'endDate': values.endDate,
            'description': values.description,
            'supplier': {
                'id': fornecedor.id,
                'name': fornecedor.name,
                'telephone': fornecedor.telephone,
                'address': fornecedor.address,
                'nif': fornecedor.nif,
            }
        })
    }).then(response => {
        return ((response.status === 200) ? getAllOrders(callback) : alert('Can not change order'))
    });
}

export function updateOrder2(values, encomendaId, fornecedor, callback) {
    fetch(`http://localhost:8080/api/v1/orders/${encomendaId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'name': values.name,
            'startDate': values.startDate,
            'endDate': values.endDate,
            'description': values.description,
            'supplier': {
                'id': fornecedor.id,
                'name': fornecedor.name,
                'telephone': fornecedor.telephone,
                'address': fornecedor.address,
                'nif': fornecedor.nif,
            }
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes2(callback) : alert('Can not change order'))
    });
}

export function updateOrder3(values, encomendaId, fornecedor, callback) {
    fetch(`http://localhost:8080/api/v1/orders/${encomendaId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'name': values.name,
            'startDate': values.startDate,
            'endDate': values.endDate,
            'description': values.description,
            'supplier': {
                'id': fornecedor.id,
                'name': fornecedor.name,
                'telephone': fornecedor.telephone,
                'address': fornecedor.address,
                'nif': fornecedor.nif,
            }
        })
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes3(callback) : alert('Can not change order'))
    });
}

//fornecedores


export function getAllSuppliers(callback) {
    fetch('http://localhost:8080/api/v1/suppliers', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function addSupplier(values, callback) {

    fetch('http://localhost:8080/api/v1/suppliers', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'name': values.name,
            'telephone': values.telephone,
            'address': values.address,
            'nif': values.nif

        })
    })
        .then(response => {
            return ((response.status === 201) ? getAllSuppliers(callback) : getAllSuppliers(callback))
        });
}


//Granites
export function getAllGranites(callback) {
    fetch('http://localhost:8080/api/v1/granites', {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => {
            return response.json()
        })
        .then(parsedJson => {
            callback(parsedJson);

        });
}

export function addGranite(values, callback) {

    fetch('http://localhost:8080/api/v1/granites', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'family': values.family,
            'type': values.type,
            'price': values.price
        })
    })
        .then(response => {
            return ((response.status === 201) ? getAllGranites(callback) : getAllGranites(callback))
        });
}


export function deleteGranite(graniteId, callback) {
    fetch(`http://localhost:8080/api/v1/granites/${graniteId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        return ((response.status === 200) ? getAllGranites(callback) : getAllGranites(callback))
    });

}


export function updateGranite(values, graniteId, callback) {
    fetch(`http://localhost:8080/api/v1/granites/${graniteId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'family': values.family,
            'type': values.type,
            'price': values.price
        })
    }).then(response => {
        return ((response.status === 200) ? getAllGranites(callback) : alert('Can not change granite'))
    });
}

//----------------------------

export function getProjectEmployees(projectId, callback) {

    fetch(`http://localhost:8080/projects/${projectId}/employees`, {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    })
        .then(response => response.json())
        .then(parsedJson => {
            callback(parsedJson);
        });
}

export function addEmployeeToProject(employeeId, allocation, projectId, callback) {
    fetch(`http://localhost:8080/employees/${employeeId}/association`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'allocation': allocation,
            "projectId": projectId
        })
    }).then(response => {
        return ((response.status === 201) ? getProjectEmployees(projectId, callback) :
            alert('The employee is already in the project!'))
    });
}

export function deleteAllocationProject(employeeId, projectId, callback) {
    fetch(`http://localhost:8080/employees/${employeeId}/projects/${projectId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        if (response.status === 200) {
            return getProjectEmployees(projectId, callback)
        }
    });
}

//users requests

export function getAllUsers(callback) {
    fetch(`http://localhost:8080/api/v1/users`, {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
    }).then(response => response.json())
        .then(parsedJson => {
            callback(parsedJson);
        });
}

export function createUser(values, callback) {
    fetch(`http://localhost:8080/api/v1/users`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'username': values.username,
            'password': values.password,
            'telephone': values.telephone,
            'address': values.address,
            'role': {
                'name': values.role.name
            }
        })
    }).then(response => {
        if (response.status === 200) {
            return getAllUsers(callback);
        } else {
            // TODO: Show error message
        }
    });
}

export function deleteUser(username, callback) {
    fetch(`http://localhost:8080/api/v1/users/${username}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        if (response.status === 200) {
            return getAllUsers(callback);
        } else {
            // TODO: Show error message
        }
    });

}

export function updateUser(values, callback) {
    fetch(`http://localhost:8080/api/v1/users`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'username': values.username,
            'telephone': values.telephone,
            'address': values.address,
            'role': {
                'name': values.role.name
            }
        })
    }).then(response => {
        if (response.status === 200) {
            return getAllUsers(callback);
        } else {
            // TODO: Show error message
        }
    });
}

export function endReception(cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/cargoes/state/id/${cargoId}`, {
        method: 'PATCH',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({})
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes2(callback) : getAllCargoes2(callback))
    });
}

export function endReception1(cargoId, callback) {
    fetch(`http://localhost:8080/api/v1/cargoes/state/id/${cargoId}`, {
        method: 'PATCH',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({})
    }).then(response => {
        return ((response.status === 200) ? getAllCargoes3(callback) : getAllCargoes3(callback))
    });
}

// ----- // ------

export function addProjectToEmployee(employeeId, allocation, projectId, callback) {

    fetch(`http://localhost:8080/employees/${employeeId}/association`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        },
        body: JSON.stringify({
            'allocation': allocation,
            "projectId": projectId
        })
    }).then(response => {
        return ((response.status === 201) ? getEmployeeProjects(employeeId, callback) :
            alert('This project is already allocated to the employee!'))
    });
}

export function getEmployeeProjects(employeeId, callback) {
    fetch(`http://localhost:8080/employees/${employeeId}/projects`, {
        headers: {
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => response.json())
        .then(parsedJson => {
            callback(parsedJson);
        });

}

export function deleteAllocationEmployee(employeeId, projectId, callback) {

    fetch(`http://localhost:8080/employees/${employeeId}/projects/${projectId}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': sessionStorage.getItem(USER_TOKEN)
        }
    }).then(response => {
        if (response.status === 200) {
            return (getEmployeeProjects(employeeId, callback))
        }
    });
}
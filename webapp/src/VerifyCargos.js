import React, {Component} from "react";
import {getAllCargoes4} from "./Requests";
import {Table} from "antd";
import './CargoesTable.css';

class VerifyCargos extends Component {

    constructor(){
        super();
        this.state={
            cargoesList:[]
        }
        this.saveList = this.saveList.bind(this);
    }
    
    componentDidMount(){
        getAllCargoes4(this.saveList);
    }

    saveList(response) {

        this.setState({cargoesList: response});

    }

    render() {

        const columns = [
            { title: 'Número da Guia', dataIndex: 'numberGuide', key: 'numberGuide' },
            { title: 'Data de receção', dataIndex: 'receptionDate', key: 'receptionDate' },
            { title: 'Peso (kg)', dataIndex: 'weight', key: 'weight' },
            { title: 'Preço (€)', dataIndex: 'price', key: 'price' },
            { title: 'Tipo de Carga', dataIndex: 'type', key: 'type' },
            { title: 'Tipo de Compra', dataIndex: 'purchaseType', key: 'purchaseType' },
            { title: 'Estado', dataIndex: 'state', key: 'state' },
        ];

        const expandedRowRender = (cargo) => {
            const columns = [
                { title: '', dataIndex: '', key: '' },
                { title: 'Bloco', dataIndex: 'blockNumber', key: 'blockNumber' },
                { title: 'Altura (m)', dataIndex: 'height', key: 'height' },
                { title: 'Comprimento (m)', dataIndex: 'length', key: 'length' },
                { title: 'Largura (m)', dataIndex: 'width', key: 'width' },
                { title: 'Qualidade do bloco', dataIndex: 'quality', key: 'quality' },
                { title: 'Avaliação do bloco', dataIndex: 'rating', key: 'rating' },
                { title: 'Descrição', dataIndex: 'description', key: 'description' },
                { title: 'Número da guia', dataIndex: 'numberGuide', key: 'numberGuide' },
            ];

            const data = cargo.blocks || [];

            return <Table columns={columns} dataSource={data} pagination={false} />;
        };

        const cargoList = this.state.cargoesList || [];

        return (
            <span>
                <Table
                    className="components-table-demo-nested"
                    columns={columns}
                    expandedRowRender={expandedRowRender}
                    dataSource={cargoList}
                />
            </span>
        );

    }
}
export default VerifyCargos;
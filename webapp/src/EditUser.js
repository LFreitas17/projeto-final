import React from 'react';
import {Button, Form, Icon, Input, Modal, Select} from 'antd';
import {updateUser} from './Requests';

const FormItem = Form.Item;
const Option = Select.Option;

const EditUserForm = Form.create()(
    class extends React.Component {
        state = {
            value: '',
        };

        onChange = (e) => {
            this.setState({
                value: e.target.value,
            });
        };

        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator} = form;
            const userInfo = this.props.data;

            return (
                <Modal
                    visible={visible}
                    title="Editar Utilizador"
                    okText="Editar"
                    onCancel={onCancel}
                    onOk={onCreate}>
                    <Form layout="vertical">
                        <FormItem label="Nome">
                            {getFieldDecorator('username', {
                                initialValue: userInfo.username,
                                rules: [{required: true, message: 'Por favor insira o nome do utilizador!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Telemóvel:">
                            {getFieldDecorator('telephone', {
                                initialValue: userInfo.telephone,
                                rules: [{required: true, message: 'Por favor insira o telemóvel do utilizador!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem label="Cargo:">
                            {getFieldDecorator('role.name', {
                                initialValue: userInfo.role ? userInfo.role.name : '',
                                rules: [{required: true, message: 'Por favor insira o cargo do utilizador!'}],
                            })(
                                <Select style={{width: 120}}>
                                    <Option value="ADMIN">Administrador</Option>
                                    <Option value="SECRETARIO">Secretário</Option>
                                    <Option value="RECECIONISTA">Rececionista</Option>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem label="Morada:">
                            {getFieldDecorator('address', {
                                initialValue: userInfo.address,
                                rules: [{required: true, message: 'Por favor insira a morada do utilizador!'}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            );
        }
    }
);

class EditUser extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {
        this.setState({visible: false});
    };

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            updateUser(values, this.props.onEdit);

            form.resetFields();
            this.setState({visible: false});
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    render() {
        return (
            <span>
        <Button onClick={this.showModal} disabled={this.props.disable}
                loading={this.props.loading}><Icon type="edit" theme="outlined"/>Editar</Button>
          <EditUserForm data={this.props.data}
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.handleCreate}
          />
      </span>
        );
    }
}

export default EditUser;

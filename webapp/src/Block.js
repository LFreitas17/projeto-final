import React, { Component } from 'react';
import CollectionsPage from './BlockEdit.js';
import { getAllBlocks } from './Requests.js';

class Block extends Component {

    constructor() {
        super();
        this.state = {
            blocks: [],
        }
     
        this.saveList=this.saveList.bind(this);
    }

    componentDidMount() {
        getAllBlocks(this.saveList);
    }

    saveList(response){
         this.setState({ blocks: response });
    }

    render() {
        const blockList = this.state.blocks || [];
        return (
                <div >
                    <CollectionsPage created={this.saveList}  blockList={blockList}/>
                </div>
            );
    }
}
export default Block;
package com.gra2003.backoffice.validator;

import com.gra2003.backoffice.model.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Override
    public boolean supports(final Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors) {
        final User user = (User) o;

        ValidationUtils.rejectIfEmpty(errors, "user", "O user encontra-se null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "O username introduzido encontra-se vazio!");
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "O username nao cumpre os requisitos minimos!");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "A password introduzida encontra-se vazia!");
        if (user.getPassword().length() < 8) {
            errors.rejectValue("password", "A password nao cumpre os requisitos minimos!");
        }
    }
}

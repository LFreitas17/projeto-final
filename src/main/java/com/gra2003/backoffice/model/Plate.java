package com.gra2003.backoffice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity(name = "plate")
public class Plate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "block_id", referencedColumnName = "id")
    private Block block;

    @OneToMany(mappedBy = "plate")
    private Collection<Product> products;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(final Block block) {
        this.block = block;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(final Collection<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Plate plate = (Plate) o;
        return id.equals(plate.id) &&
                Objects.equals(description, plate.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description);
    }
}

package com.gra2003.backoffice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity(name = "block")
public class Block {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "block_number")
    private Integer blockNumber;

    @Basic
    @Column(name = "quality")
    @Enumerated(EnumType.STRING)
    private Quality quality;

    @Basic
    @Column(name = "rating")
    @Enumerated(EnumType.STRING)
    private Rating rating;

    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "height")
    private Float height;

    @Basic
    @Column(name = "length")
    private Float length;

    @Basic
    @Column(name = "width")
    private Float width;

    @ManyToOne
    @JoinColumn(name = "cargo_id", referencedColumnName = "id")
    private Cargo cargo;

    @ManyToOne
    @JoinColumn(name = "granite_id", referencedColumnName = "id")
    private Granite granite;

    @OneToMany(mappedBy = "block")
    private Collection<Plate> plates;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(final Integer blockNumber) {
        this.blockNumber = blockNumber;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(final Quality block) {
        this.quality = block;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(final Rating rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(final Float height) {
        this.height = height;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(final Float lenght) {
        this.length = lenght;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(final Float width) {
        this.width = width;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(final Cargo cargo) {
        this.cargo = cargo;
    }

    public Granite getGranite() {
        return granite;
    }

    public void setGranite(final Granite granite) {
        this.granite = granite;
    }

    public Collection<Plate> getPlates() {
        return plates;
    }

    public void setPlates(final Collection<Plate> plates) {
        this.plates = plates;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Block block = (Block) o;
        return Objects.equals(id, block.id)
                && Objects.equals(blockNumber, block.blockNumber)
                && Objects.equals(quality, block.quality)
                && Objects.equals(description, block.description)
                && Objects.equals(height, block.height)
                && Objects.equals(length, block.length)
                && Objects.equals(width, block.width);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, blockNumber, quality, description, height, length, width);
    }
}

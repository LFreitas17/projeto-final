package com.gra2003.backoffice.model;

public enum LoadState {
    NORMAL, NOT_FINISHED, FINISHED, VERIFIED
}

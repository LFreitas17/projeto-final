package com.gra2003.backoffice.model;

public enum PurchaseType {
    BY_BLOCK, BY_LOAD
}

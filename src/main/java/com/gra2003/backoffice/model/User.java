package com.gra2003.backoffice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic
    @Column(name = "username")
    private String username;

    @Basic
    @Column(name = "password")
    private String password;

    @Basic
    @Column(name = "telephone")
    private String telephone;

    @Basic
    @Column(name = "adress")
    private String address;

    @OneToMany(mappedBy = "userCreatedBy")
    private Collection<Order> ordersCreatedBy;

    @OneToMany(mappedBy = "userLastEditedBy")
    private Collection<Order> ordersLastEditedBy;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private Role role;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String name) {
        this.username = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public Collection<Order> getOrdersCreatedBy() {
        return ordersCreatedBy;
    }

    public void setOrdersCreatedBy(final Collection<Order> ordersCreatedBy) {
        this.ordersCreatedBy = ordersCreatedBy;
    }

    public Collection<Order> getOrdersLastEditedBy() {
        return ordersLastEditedBy;
    }

    public void setOrdersLastEditedBy(final Collection<Order> ordersLastEditedBy) {
        this.ordersLastEditedBy = ordersLastEditedBy;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final User user = (User) o;
        return Objects.equals(id, user.id)
                && Objects.equals(username, user.username)
                && Objects.equals(password, user.password)
                && Objects.equals(telephone, user.telephone)
                && Objects.equals(address, user.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, telephone, address);
    }
}

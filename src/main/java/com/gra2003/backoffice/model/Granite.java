package com.gra2003.backoffice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity(name = "granite")
public class Granite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "family")
    private String family;

    @Basic
    @Column(name = "type")
    private String type;

    //preço por metro cúbico
    @Basic
    @Column(name = "price")
    private Float price;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(final String family) {
        this.family = family;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(final Float price) {
        this.price = price;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Granite granite = (Granite) o;
        return id.equals(granite.id) &&
                Objects.equals(family, granite.family) &&
                Objects.equals(type, granite.type) &&
                Objects.equals(price, granite.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, family, type, price);
    }
}

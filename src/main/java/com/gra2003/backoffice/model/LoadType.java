package com.gra2003.backoffice.model;

public enum LoadType {
    EUROS_TON, EUROS_MCUB, EUROS_LOAD
}

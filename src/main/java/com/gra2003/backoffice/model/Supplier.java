package com.gra2003.backoffice.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity(name = "supplier")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "telephone")
    private String telephone;

    @Basic
    @Column(name = "address")
    private String address;

    @Basic
    @Column(name = "nif")
    private String nif;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    private List<Order> orders;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(final String nif) {
        this.nif = nif;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(final List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Supplier supplier = (Supplier) o;
        return id.equals(supplier.id) &&
                Objects.equals(name, supplier.name) &&
                Objects.equals(telephone, supplier.telephone) &&
                Objects.equals(address, supplier.address) &&
                Objects.equals(nif, supplier.nif);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, telephone, address, nif);
    }

}

package com.gra2003.backoffice.model;

import javax.persistence.*;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity(name = "cargo")
public class Cargo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic
    @Column(name = "reception_date")
    private Date receptionDate;

    @Basic
    @Column(name = "number_guide")
    private String numberGuide;

    @Basic
    @Column(name = "weight")
    private Float weight;

    @Basic
    @Column(name = "price")
    private Float price;

    @Basic
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private LoadType type;

    @Basic
    @Column(name = "purchase_type")
    @Enumerated(EnumType.STRING)
    private PurchaseType purchaseType;

    @Basic
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private LoadState state;

    @OneToMany(mappedBy = "cargo",cascade=CascadeType.ALL)
    private List<Block> blocks;

    @ManyToOne
    @JoinColumn(name = "client_order_id", referencedColumnName = "id")
    private Order order;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Date getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(final Date receptionDate) {
        this.receptionDate = receptionDate;
    }

    public String getNumberGuide() {
        return numberGuide;
    }

    public void setNumberGuide(final String numberGuide) {
        this.numberGuide = numberGuide;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(final Float weight) {
        this.weight = weight;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(final Float price) {
        this.price = price;
    }

    public LoadType getType() {
        return type;
    }

    public void setType(final LoadType type) {
        this.type = type;
    }

    public PurchaseType getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(final PurchaseType purchaseType) {
        this.purchaseType = purchaseType;
    }

    public LoadState getState() {
        return state;
    }

    public void setState(final LoadState state) {
        this.state = state;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(final List<Block> blocks) {
        this.blocks = blocks;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(final Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Cargo cargo = (Cargo) o;
        return id.equals(cargo.id) &&
                Objects.equals(receptionDate, cargo.receptionDate) &&
                Objects.equals(numberGuide, cargo.numberGuide) &&
                Objects.equals(weight, cargo.weight) &&
                Objects.equals(price, cargo.price) &&
                Objects.equals(type, cargo.type) &&
                Objects.equals(purchaseType, cargo.purchaseType) &&
                Objects.equals(state, cargo.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, receptionDate, numberGuide, weight, price, type, purchaseType, state);
    }
}

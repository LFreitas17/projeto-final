package com.gra2003.backoffice.mapper;

import com.gra2003.backoffice.dto.GraniteDTO;
import com.gra2003.backoffice.model.Granite;
import org.mapstruct.Mapper;

import java.util.Collection;
import java.util.List;

@Mapper
public interface GraniteMapper {

    Granite mapGraniteDtoToGranite(GraniteDTO granite);

    List<Granite> mapGranitesDtoToGranites(List<GraniteDTO> granites);

    GraniteDTO mapGraniteToDto(Granite granite);

    List<GraniteDTO> mapGranitesToDto(List<Granite> granites);
}

package com.gra2003.backoffice.mapper;


import com.gra2003.backoffice.dto.SupplierDTO;
import com.gra2003.backoffice.model.Supplier;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface SupplierMapper {

    Supplier mapSupplierDtoToSupplier(SupplierDTO supplier);

    List<Supplier> mapSuppliersDtoToSuppliers(List<SupplierDTO> suppliers);

    SupplierDTO mapSupplierToDto(Supplier supplier);

    List<SupplierDTO> mapSuppliersToDto(List<Supplier> suppliers);
}

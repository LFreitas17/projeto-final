package com.gra2003.backoffice.mapper;

import com.gra2003.backoffice.dto.CargoDTO;
import com.gra2003.backoffice.model.Cargo;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CargoMapper {
    Cargo mapCargoDtoToCargo(CargoDTO cargo);

    List<Cargo> mapCargoesDtoToCargo(List<CargoDTO> cargoes);

    CargoDTO mapCargoToDto(Cargo cargo);

    List<CargoDTO> mapCargoesToDto(List<Cargo> cargoes);
}

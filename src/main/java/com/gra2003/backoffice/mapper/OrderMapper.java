package com.gra2003.backoffice.mapper;

import com.gra2003.backoffice.dto.OrderDTO;
import com.gra2003.backoffice.model.Order;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {

    Order mapOrderDtoToOrder(OrderDTO order);

    List<Order> mapOrdersDtoToOrders(List<OrderDTO> orders);

    OrderDTO mapOrderToDto(Order order);

    List<OrderDTO> mapOrdersToDto(List<Order> orders);
}

package com.gra2003.backoffice.mapper;

import com.gra2003.backoffice.dto.BlockDTO;
import com.gra2003.backoffice.model.Block;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface BlockMapper {

    Block mapBlockDtoToBlock(BlockDTO block);

    List<Block> mapBlocksDtoToBlocks(List<BlockDTO> blocks);

    BlockDTO mapBlockToDto(Block block);

    List<BlockDTO> mapBlocksToDto(List<Block> blocks);

}

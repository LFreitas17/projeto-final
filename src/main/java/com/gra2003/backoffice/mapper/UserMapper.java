package com.gra2003.backoffice.mapper;

import com.gra2003.backoffice.dto.UserDTO;
import com.gra2003.backoffice.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface UserMapper {
    User mapUserDtoToUser(UserDTO user);

    List<User> mapUsersDtoToUsers(List<UserDTO> users);

    @Mapping(target = "password", ignore = true)
    UserDTO mapUserToDto(User user);

    List<UserDTO> mapUsersToDto(List<User> users);
}

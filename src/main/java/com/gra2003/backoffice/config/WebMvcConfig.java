package com.gra2003.backoffice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Create the following WebMvcConfig class inside project.config package to enable cross origin requests globally.
 * The allowed methods are: PUT, DELETE, GET and POST.
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("PUT", "DELETE", "GET", "POST")
                .allowedOrigins("http://localhost:3000");
    }
}

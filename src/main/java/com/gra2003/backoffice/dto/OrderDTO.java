package com.gra2003.backoffice.dto;


import java.sql.Date;
import java.util.List;

public class OrderDTO {

    private Integer id;
    private String name;
    private Date startDate;
    private Date endDate;
    private String description;
    private SupplierDTO supplier;
    private List<CargoDTO> cargoes;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public SupplierDTO getSupplier() {
        return supplier;
    }

    public void setSupplier(final SupplierDTO supplier) {
        this.supplier = supplier;
    }

    public List<CargoDTO> getCargoes() {
        return cargoes;
    }

    public void setCargoes(final List<CargoDTO> cargoes) {
        this.cargoes = cargoes;
    }
}

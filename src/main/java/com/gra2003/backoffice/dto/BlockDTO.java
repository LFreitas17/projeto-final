package com.gra2003.backoffice.dto;

import com.gra2003.backoffice.model.Quality;
import com.gra2003.backoffice.model.Rating;

public class BlockDTO {
    private Integer id;
    private Integer blockNumber;
    private Quality quality;
    private Rating rating;
    private String description;
    private Float height;
    private Float length;
    private Float width;
    private GraniteDTO granite;
    private String numberOfGuide;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(final Integer blockNumber) {
        this.blockNumber = blockNumber;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(final Quality quality) {
        this.quality = quality;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(final Rating rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(final Float height) {
        this.height = height;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(final Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(final Float width) {
        this.width = width;
    }

    public GraniteDTO getGranite() {
        return granite;
    }

    public void setGranite(final GraniteDTO granite) {
        this.granite = granite;
    }

    public String getNumberOfGuide() {
        return numberOfGuide;
    }

    public void setNumberOfGuide(final String numberOfGuide) {
        this.numberOfGuide = numberOfGuide;
    }
}

package com.gra2003.backoffice.dto;

public class RoleDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}

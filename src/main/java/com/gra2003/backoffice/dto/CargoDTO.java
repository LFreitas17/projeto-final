package com.gra2003.backoffice.dto;

import com.gra2003.backoffice.model.LoadState;
import com.gra2003.backoffice.model.LoadType;
import com.gra2003.backoffice.model.PurchaseType;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

public class CargoDTO {
    private Integer id;
    private Date receptionDate;
    private String numberGuide;
    private Float weight;
    private Float price;
    private LoadType type;
    private PurchaseType purchaseType;
    private LoadState state;
    private List<BlockDTO> blocks;
    private String nameOrder;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Date getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(final Date receptionDate) {
        this.receptionDate = receptionDate;
    }

    public String getNumberGuide() {
        return numberGuide;
    }

    public void setNumberGuide(final String numberGuide) {
        this.numberGuide = numberGuide;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(final Float weight) {
        this.weight = weight;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(final Float price) {
        this.price = price;
    }

    public LoadType getType() {
        return type;
    }

    public void setType(final LoadType type) {
        this.type = type;
    }

    public PurchaseType getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(final PurchaseType purchaseType) {
        this.purchaseType = purchaseType;
    }

    public LoadState getState() {
        return state;
    }

    public void setState(final LoadState state) {
        this.state = state;
    }

    public List<BlockDTO> getBlocks() {
        return blocks;
    }

    public void setBlocks(final List<BlockDTO> blocks) {
        this.blocks = blocks;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public void setNameOrder(final String nameOrder) {
        this.nameOrder = nameOrder;
    }
}

package com.gra2003.backoffice.dto;

public class SupplierDTO {

    private Integer id;
    private String name;
    private String telephone;
    private String address;
    private String nif;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(final String nif) {
        this.nif = nif;
    }

}

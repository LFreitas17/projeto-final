package com.gra2003.backoffice.service.api;

import com.gra2003.backoffice.dto.BlockDTO;

import java.util.List;
import java.util.Map;

public interface BlockService {
    List<BlockDTO> getAllBlocks();

    BlockDTO getBlockById(final Integer blockId);

    BlockDTO createBlock(final BlockDTO blockDTO);

    BlockDTO updateBlock(final Integer id, final BlockDTO blockDTO);

    Map<String, Boolean> deleteBlock(final Integer blockId);

    List<BlockDTO> getBlockByIdCargo(final Integer blockId);

//    /**
//     * Calculated with the formula V = L * H * W
//     * @param blockId the of the block
//     * @return the volume of a block
//     */
//    Float getBlockVolume(final Integer blockId);
}

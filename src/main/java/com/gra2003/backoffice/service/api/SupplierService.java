package com.gra2003.backoffice.service.api;

import com.gra2003.backoffice.dto.SupplierDTO;

import java.util.List;
import java.util.Map;

public interface SupplierService {

    List<SupplierDTO> getAllSupplier();

    SupplierDTO getSupplierById(final Integer supplierId);

    SupplierDTO createSupplier(final SupplierDTO supplierDTO);

    SupplierDTO updateSupplier(final Integer id, final SupplierDTO supplierDTO);

    Map<String, Boolean> deleteSupplier(final Integer supplierId);
}

package com.gra2003.backoffice.service.api;


import com.gra2003.backoffice.dto.OrderDTO;

import java.util.List;
import java.util.Map;

public interface OrderService {

    List<OrderDTO> getAllOrder();

    OrderDTO getOrderById(final Integer orderId);

    OrderDTO createOrder(final OrderDTO orderDTO);

    OrderDTO updateOrder(final Integer id, final OrderDTO orderDTO);

    Map<String, Boolean> deleteOrder(final Integer orderId);

    OrderDTO getOrderDTOByNameOrder(final String numberGuide);

}

package com.gra2003.backoffice.service.api;

import com.gra2003.backoffice.dto.CargoDTO;
import com.gra2003.backoffice.model.LoadState;

import java.util.List;
import java.util.Map;

public interface CargoService {

    List<CargoDTO> getAllCargoes();

    List<CargoDTO> getAllCargoesByState();

    List<CargoDTO> getAllCargoesByState2(final LoadState state);

    CargoDTO changeStateByCargoId(final Integer cargoId);

    CargoDTO getCargoById(final Integer cargoId);

    CargoDTO createCargo(final CargoDTO cargoDto);

    CargoDTO updateCargo(final Integer id, final CargoDTO cargoDTO);

    Map<String, Boolean> deleteCargo(final Integer cargoId);

    CargoDTO getCargoDTOByNumberGuide(final String numberGuide);

    CargoDTO updateCargoByAddOrder(final Integer orderId, final CargoDTO cargoDTO);

    Boolean existNumberOfGuide(final String numberGuide);
}

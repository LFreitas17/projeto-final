package com.gra2003.backoffice.service.api;


import com.gra2003.backoffice.dto.GraniteDTO;

import java.util.List;
import java.util.Map;

public interface GraniteService {
    List<GraniteDTO> getAllGranites();

    GraniteDTO getGraniteById(final Integer id);

    GraniteDTO createGranite(final GraniteDTO graniteDto);

    GraniteDTO updateGranite(final Integer id, final GraniteDTO graniteDTO);

    Map<String, Boolean> deleteGranite(final Integer graniteId);
}

package com.gra2003.backoffice.service.api;

import com.gra2003.backoffice.dto.UserDTO;
import com.gra2003.backoffice.exceptions.RoleNotFoundException;
import com.gra2003.backoffice.exceptions.UsernameAlreadyExists;
import com.gra2003.backoffice.exceptions.UsernameNotFoundException;

import java.util.List;

public interface UserService {
    List<UserDTO> getAllUsers();

    UserDTO findByUsername(String username) throws UsernameNotFoundException;

    void createUser(final UserDTO userDto) throws UsernameAlreadyExists, RoleNotFoundException;

    void updateUser(UserDTO userDto) throws UsernameNotFoundException, RoleNotFoundException;

    void deleteUser(String username) throws UsernameNotFoundException;

    boolean loggedUserIsAdmin();

    String getLoggedUsername();
}

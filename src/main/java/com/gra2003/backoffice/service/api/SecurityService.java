package com.gra2003.backoffice.service.api;

import com.gra2003.backoffice.exceptions.UsernameNotFoundException;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password) throws UsernameNotFoundException;
}

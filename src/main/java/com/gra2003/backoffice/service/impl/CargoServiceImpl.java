package com.gra2003.backoffice.service.impl;

import com.gra2003.backoffice.dto.CargoDTO;
import com.gra2003.backoffice.dto.OrderDTO;
import com.gra2003.backoffice.mapper.CargoMapper;
import com.gra2003.backoffice.mapper.OrderMapper;
import com.gra2003.backoffice.model.Cargo;
import com.gra2003.backoffice.model.LoadState;
import com.gra2003.backoffice.model.Order;
import com.gra2003.backoffice.repository.CargoRepository;
import com.gra2003.backoffice.repository.OrderRepository;
import com.gra2003.backoffice.service.api.CargoService;
import com.gra2003.backoffice.service.api.OrderService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Service
public class CargoServiceImpl implements CargoService {

    @Autowired
    private CargoRepository cargoRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    private CargoMapper mapper = Mappers.getMapper(CargoMapper.class);

    private OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);

    public List<CargoDTO> getAllCargoes() {
        final List<Cargo> cargoes = cargoRepository.findAll();
        return pasteOrderName(cargoes);
    }

    public List<CargoDTO> getAllCargoesByState() {
        final List<Cargo> cargoes = cargoRepository.findAllCargoesByState();
        return pasteOrderName(cargoes);
    }

    public List<CargoDTO> getAllCargoesByState2(final LoadState state) {
        final List<Cargo> cargoes = cargoRepository.findAllCargoesByState2(state);
        return pasteOrderName(cargoes);
    }

    private List<CargoDTO> pasteOrderName(final List<Cargo> cargoes) {
        List<CargoDTO> cargoDTO = mapper.mapCargoesToDto(cargoes);
        for(int p=0; p<cargoes.size(); p++){
            if(cargoes.get(p).getOrder() == null){
                continue;
            }else {
                if (cargoes.get(p).getOrder().getName() != null && !"".equals(cargoes.get(p).getOrder().getName())) {
                    cargoDTO.get(p).setNameOrder(cargoes.get(p).getOrder().getName());
                }
            }
        }
        return cargoDTO;
    }

    public CargoDTO changeStateByCargoId(final Integer cargoId){
        final Cargo cargo = this.getCargoByIdFromRepository(cargoId);

        if(LoadState.NORMAL.equals(cargo.getState())){
            cargo.setState(LoadState.NOT_FINISHED);
        }else if(LoadState.NOT_FINISHED.equals(cargo.getState())){
            cargo.setState(LoadState.FINISHED);
        }else if(LoadState.FINISHED.equals(cargo.getState())){
            cargo.setState(LoadState.VERIFIED);
        }
        cargoRepository.save(cargo);
        final CargoDTO cargoDTO = mapper.mapCargoToDto(cargo);
        if(cargo.getOrder()!=null) {
            cargoDTO.setNameOrder(cargo.getOrder().getName());
        }
        return cargoDTO;
    }

    public CargoDTO getCargoById(final Integer cargoId) {
        final Cargo cargo = this.getCargoByIdFromRepository(cargoId);
        final CargoDTO cargoDTO = mapper.mapCargoToDto(cargo);
        if(cargo.getOrder() !=null) {
            cargoDTO.setNameOrder(cargo.getOrder().getName());
        }
        return cargoDTO;
    }

    private Cargo getCargoByIdFromRepository(final Integer id) {
        return cargoRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Cargo not found on :: " + id));
    }

    public CargoDTO createCargo(final CargoDTO cargoDTO) {
        Order order = new Order();
       // if(this.getCargoDTOByNumberGuide(cargoDTO.getNumberGuide())==null) {
            if (cargoDTO.getNameOrder() != null && !"".equals(cargoDTO.getNameOrder())) {
                order = orderMapper.mapOrderDtoToOrder(orderService.getOrderDTOByNameOrder(cargoDTO.getNameOrder()));
            }
            final Cargo cargo = mapper.mapCargoDtoToCargo(cargoDTO);
            if (cargoDTO.getNameOrder() != null && !"".equals(cargoDTO.getNameOrder())) {
                cargo.setOrder(order);
            }
            cargoRepository.save(cargo);
            return getCargoById(cargo.getId());
     //   }

    }

    public CargoDTO updateCargo(final Integer id, final CargoDTO cargoDTO) {
        Order order = new Order();
        if (cargoDTO.getNameOrder() != null && !"".equals(cargoDTO.getNameOrder())) {
            order = orderMapper.mapOrderDtoToOrder(orderService.getOrderDTOByNameOrder(cargoDTO.getNameOrder()));
        }
        final Cargo cargo = this.getCargoByIdFromRepository(id);
        cargo.setReceptionDate(cargoDTO.getReceptionDate());
        cargo.setNumberGuide(cargoDTO.getNumberGuide());
        cargo.setWeight(cargoDTO.getWeight());
        cargo.setPrice(cargoDTO.getPrice());
        cargo.setType(cargoDTO.getType());
        cargo.setPurchaseType(cargoDTO.getPurchaseType());
        if(cargoDTO.getState()==null || LoadState.NORMAL.equals(cargoDTO.getState()) ||
                LoadState.NOT_FINISHED.equals(cargoDTO.getState()) || LoadState.FINISHED.equals(cargoDTO.getState()) ||
                LoadState.VERIFIED.equals(cargoDTO.getState())){
            cargo.setState(cargo.getState());
        }else{
            cargo.setState(cargoDTO.getState());
        }

        if (cargoDTO.getNameOrder() != null && !"".equals(cargoDTO.getNameOrder())) {
            cargo.setOrder(order);
        }
        cargoRepository.save(cargo);
        return getCargoById(cargo.getId());
    }

    public Map<String, Boolean> deleteCargo(final Integer id) {
        final Cargo cargo = this.getCargoByIdFromRepository(id);
        cargoRepository.delete(cargo);

        // TODO: Why return a Map? Wouldn't it be enough a boolean?
        final Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    public CargoDTO getCargoDTOByNumberGuide(final String numberGuide) {
        final Cargo cargo = cargoRepository.findCargoByNumberGuide(numberGuide);
        return mapper.mapCargoToDto(cargo);
    }

    public CargoDTO updateCargoByAddOrder(final Integer orderId, final CargoDTO cargoDTO) {
        final Order order = this.getOrderByIdFromRepository(orderId);
        final Cargo cargo = this.getCargoByIdFromRepository(cargoDTO.getId());
        cargo.setOrder(order);
        final CargoDTO newCargoDTO = getCargoById(cargo.getId());
        newCargoDTO.setNameOrder(order.getName());
        return newCargoDTO;
    }

    private Order getOrderByIdFromRepository(final Integer id) {
        return orderRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Order not found on :: " + id));
    }

    public Boolean existNumberOfGuide(final String numberGuide){
Cargo cargo = cargoRepository.findCargoByNumberGuide(numberGuide);
        if( cargo == null || ("").equals(cargo)){
            return false;
        }
        return true;
    }


    private CargoDTO newDateFormato(){
        CargoDTO cargoTmp = new CargoDTO();

        return cargoTmp;
    }
}

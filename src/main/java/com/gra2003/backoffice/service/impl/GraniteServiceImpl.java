package com.gra2003.backoffice.service.impl;

import com.gra2003.backoffice.dto.GraniteDTO;
import com.gra2003.backoffice.mapper.GraniteMapper;
import com.gra2003.backoffice.model.Granite;
import com.gra2003.backoffice.repository.GraniteRepository;
import com.gra2003.backoffice.service.api.GraniteService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GraniteServiceImpl implements GraniteService {

    @Autowired
    private GraniteRepository repository;

    private GraniteMapper mapper = Mappers.getMapper(GraniteMapper.class);

    public List<GraniteDTO> getAllGranites() {
        final List<Granite> granites = repository.findAll();
        return mapper.mapGranitesToDto(granites);
    }


    public GraniteDTO getGraniteById(final Integer id) {
        final Granite granite = this.getGraniteByIdFromRepository(id);
        return mapper.mapGraniteToDto(granite);
    }

    private Granite getGraniteByIdFromRepository(final Integer id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Granite not found on :: " + id));
    }

    public GraniteDTO createGranite(final GraniteDTO graniteDto) {
        final Granite granite = mapper.mapGraniteDtoToGranite(graniteDto);
        final Granite savedGranite = repository.save(granite);
        return mapper.mapGraniteToDto(savedGranite);
    }

    public GraniteDTO updateGranite(final Integer id, final GraniteDTO graniteDTO) {
        final Granite granite = this.getGraniteByIdFromRepository(id);
        granite.setFamily(graniteDTO.getFamily());
        granite.setType(graniteDTO.getType());
        granite.setPrice(graniteDTO.getPrice());
        final Granite savedGranite = repository.save(granite);
        return mapper.mapGraniteToDto(savedGranite);
    }

    public Map<String, Boolean> deleteGranite(final Integer id) {
        final Granite granite = this.getGraniteByIdFromRepository(id);
        repository.delete(granite);

        // TODO: Why return a Map? Wouldn't it be enough a boolean?
        final Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

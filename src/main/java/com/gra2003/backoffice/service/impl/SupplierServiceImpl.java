package com.gra2003.backoffice.service.impl;

import com.gra2003.backoffice.dto.SupplierDTO;
import com.gra2003.backoffice.mapper.SupplierMapper;
import com.gra2003.backoffice.model.Supplier;
import com.gra2003.backoffice.repository.SupplierRepository;
import com.gra2003.backoffice.service.api.SupplierService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    private SupplierMapper supplierMapper = Mappers.getMapper(SupplierMapper.class);

    public List<SupplierDTO> getAllSupplier(){
        final List<Supplier> suppliers = supplierRepository.findAll();
        return supplierMapper.mapSuppliersToDto(suppliers);
    }

    public SupplierDTO getSupplierById(final Integer supplierId){
        final Supplier supplier = this.getSupplierByIdFromRepository(supplierId);
        return supplierMapper.mapSupplierToDto(supplier);
    }

    public SupplierDTO createSupplier(final SupplierDTO supplierDTO){
        final Supplier supplier = supplierMapper.mapSupplierDtoToSupplier(supplierDTO);
        final Supplier savedSupplier = supplierRepository.save(supplier);
        return supplierMapper.mapSupplierToDto(savedSupplier);
    }

    public SupplierDTO updateSupplier(final Integer id, final SupplierDTO supplierDTO){
        final Supplier supplier = this.getSupplierByIdFromRepository(id);
        supplier.setName(supplierDTO.getName());
        supplier.setTelephone(supplierDTO.getTelephone());
        supplier.setAddress(supplierDTO.getAddress());
        supplier.setNif(supplierDTO.getNif());
        final Supplier savedSupplier = supplierRepository.save(supplier);
        return supplierMapper.mapSupplierToDto(savedSupplier);
    }

    public Map<String, Boolean> deleteSupplier(final Integer supplierId){
        final Supplier supplier = this.getSupplierByIdFromRepository(supplierId);
        supplierRepository.delete(supplier);

        // TODO: Why return a Map? Wouldn't it be enough a boolean?
        final Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    private Supplier getSupplierByIdFromRepository(final Integer id) {
        return supplierRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Supplier not found on :: " + id));
    }


}

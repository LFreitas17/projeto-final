package com.gra2003.backoffice.service.impl;

import com.gra2003.backoffice.dto.OrderDTO;
import com.gra2003.backoffice.mapper.OrderMapper;
import com.gra2003.backoffice.mapper.SupplierMapper;
import com.gra2003.backoffice.model.Order;
import com.gra2003.backoffice.model.Supplier;
import com.gra2003.backoffice.repository.OrderRepository;
import com.gra2003.backoffice.repository.SupplierRepository;
import com.gra2003.backoffice.service.api.OrderService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private SupplierRepository supplierRepository;

    private OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);
    private SupplierMapper supplierMapper = Mappers.getMapper(SupplierMapper.class);

    public List<OrderDTO> getAllOrder(){
        final List<Order> orders = orderRepository.findAll();
        return orderMapper.mapOrdersToDto(orders);
    }

    public OrderDTO getOrderById(final Integer orderId){
        final Order order = this.getOrderByIdFromRepository(orderId);
        return orderMapper.mapOrderToDto(order);
    }

    public OrderDTO createOrder(final OrderDTO orderDTO){
        final Order order = orderMapper.mapOrderDtoToOrder(orderDTO);
        final Order savedOrder = orderRepository.save(order);
        return orderMapper.mapOrderToDto(savedOrder);
    }

    public OrderDTO updateOrder(final Integer id, final OrderDTO orderDTO){
        final Order order = this.getOrderByIdFromRepository(id);
        order.setName(orderDTO.getName());
        order.setStartDate(orderDTO.getStartDate());
        order.setEndDate(orderDTO.getEndDate());
        order.setDescription(orderDTO.getDescription());
       // final Supplier supplier = this.getSupplierByIdFromRepository(order.getSupplier().getId())
        order.setSupplier(supplierMapper.mapSupplierDtoToSupplier(orderDTO.getSupplier()));
        final Order savedOrder = orderRepository.save(order);
        return orderMapper.mapOrderToDto(savedOrder);
    }

    public Map<String, Boolean> deleteOrder(final Integer orderId){
        final Order order = this.getOrderByIdFromRepository(orderId);
        orderRepository.delete(order);

        // TODO: Why return a Map? Wouldn't it be enough a boolean?
        final Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    private Order getOrderByIdFromRepository(final Integer id) {
        return orderRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Order not found on :: " + id));
    }

    private Supplier getSupplierByIdFromRepository(final Integer id) {
        return supplierRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Supplier not found on :: " + id));
    }

    public OrderDTO getOrderDTOByNameOrder(final String nameOrder){
        final Order order = orderRepository.findOrderByName(nameOrder);
        return orderMapper.mapOrderToDto(order);
    }
}

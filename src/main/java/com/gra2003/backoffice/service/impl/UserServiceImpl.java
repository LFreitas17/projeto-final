package com.gra2003.backoffice.service.impl;

import com.gra2003.backoffice.dto.UserDTO;
import com.gra2003.backoffice.exceptions.RoleNotFoundException;
import com.gra2003.backoffice.exceptions.UsernameAlreadyExists;
import com.gra2003.backoffice.exceptions.UsernameNotFoundException;
import com.gra2003.backoffice.mapper.UserMapper;
import com.gra2003.backoffice.model.Role;
import com.gra2003.backoffice.model.RoleValues;
import com.gra2003.backoffice.model.User;
import com.gra2003.backoffice.repository.RoleRepository;
import com.gra2003.backoffice.repository.UserRepository;
import com.gra2003.backoffice.service.api.UserService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private UserMapper mapper = Mappers.getMapper(UserMapper.class);

    @Override
    public List<UserDTO> getAllUsers() {
        final List<User> users = userRepository.findAll();
        return mapper.mapUsersToDto(users);
    }

    @Override
    public UserDTO findByUsername(final String username) throws UsernameNotFoundException {
        final User user = this.getUserByUsernameFromRepository(username);
        return mapper.mapUserToDto(user);
    }

    @Override
    public void createUser(final UserDTO userDto) throws UsernameAlreadyExists, RoleNotFoundException {
        if (userRepository.findByUsername(userDto.getUsername()) != null) {
            throw new UsernameAlreadyExists(userDto.getUsername());
        }
        final Role role = roleRepository.findByName(userDto.getRole().getName());
        if (role == null) {
            throw new RoleNotFoundException(userDto.getRole().getName());
        }

        final User user = mapper.mapUserDtoToUser(userDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    public void updateUser(final UserDTO userDto) throws UsernameNotFoundException, RoleNotFoundException {
        final User user = this.getUserByUsernameFromRepository(userDto.getUsername());
        user.setUsername(userDto.getUsername());
        user.setTelephone(userDto.getTelephone());
        user.setAddress(userDto.getAddress());

        final Role role = roleRepository.findByName(userDto.getRole().getName());
        if (role == null) {
            throw new RoleNotFoundException(userDto.getRole().getName());
        }
        user.setRole(role);

        userRepository.save(user);
    }

    @Override
    public void deleteUser(final String username) throws UsernameNotFoundException {
        final User user = this.getUserByUsernameFromRepository(username);
        userRepository.delete(user);
    }

    @Override
    public boolean loggedUserIsAdmin() {
        final Authentication authDetails = SecurityContextHolder.getContext().getAuthentication();
        return authDetails.getAuthorities().stream()
                .anyMatch(authority -> authority.getAuthority().equals(RoleValues.ADMIN.name())
                        || authority.getAuthority().equals(RoleValues.SECRETARIO.name()));
    }

    @Override
    public String getLoggedUsername() {
        final Authentication authDetails = SecurityContextHolder.getContext().getAuthentication();
        return ((UserDetails) authDetails.getPrincipal()).getUsername();
    }

    private User getUserByUsernameFromRepository(final String username) throws UsernameNotFoundException {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }
}

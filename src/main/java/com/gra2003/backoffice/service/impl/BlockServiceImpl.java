package com.gra2003.backoffice.service.impl;

import com.gra2003.backoffice.dto.BlockDTO;
import com.gra2003.backoffice.mapper.BlockMapper;
import com.gra2003.backoffice.mapper.CargoMapper;
import com.gra2003.backoffice.mapper.GraniteMapper;
import com.gra2003.backoffice.model.Block;
import com.gra2003.backoffice.model.Cargo;
import com.gra2003.backoffice.model.Granite;
import com.gra2003.backoffice.repository.BlockRepository;
import com.gra2003.backoffice.repository.GraniteRepository;
import com.gra2003.backoffice.service.api.BlockService;
import com.gra2003.backoffice.service.api.CargoService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Service
public class BlockServiceImpl implements BlockService {

    @Autowired
    private BlockRepository blockRepository;

    @Autowired
    private GraniteRepository graniteRepository;

    @Autowired
    private CargoService cargoService;

    private BlockMapper blockMapper = Mappers.getMapper(BlockMapper.class);

    private CargoMapper cargoMapper = Mappers.getMapper(CargoMapper.class);

    private GraniteMapper graniteMapper = Mappers.getMapper(GraniteMapper.class);

    public List<BlockDTO> getAllBlocks() {
        final List<Block> blocks = blockRepository.findAll();
        final List<BlockDTO> blockDTO = blockMapper.mapBlocksToDto(blocks);
        IntStream.range(0, blocks.size()).filter(i -> blocks.get(i).getCargo() != null).forEach(i -> blockDTO.get(i).setNumberOfGuide(blocks.get(i).getCargo().getNumberGuide()));
        return blockDTO;
    }

    public BlockDTO getBlockById(final Integer blockId) {
        final Block block = this.getBlockByIdFromRepository(blockId);
        final BlockDTO blockDTO = blockMapper.mapBlockToDto(block);
        if(block.getCargo()!=null) {
            blockDTO.setNumberOfGuide(block.getCargo().getNumberGuide());
        }
        return blockDTO;
    }

    public BlockDTO createBlock(final BlockDTO blockDTO) {
        Cargo cargo = new Cargo();
        if(blockDTO.getNumberOfGuide()!=null && !"".equals(blockDTO.getNumberOfGuide())) {
            cargo = cargoMapper.mapCargoDtoToCargo(cargoService.getCargoDTOByNumberGuide(blockDTO.getNumberOfGuide()));
        }
        final Block block = blockMapper.mapBlockDtoToBlock(blockDTO);
        if(blockDTO.getNumberOfGuide()!=null && !"".equals(blockDTO.getNumberOfGuide())) {
            block.setCargo(cargo);
        }
        blockRepository.save(block);
        return getBlockById(block.getId());
    }

//    public BlockDTO createBlock2(final BlockDTO detailsBlock) {
//        final Granite granite;
//        Cargo cargo = new Cargo();
//        if(detailsBlock.getNumberOfGuide()!=null && !"".equals(detailsBlock.getNumberOfGuide())) {
//            cargo = cargoMapper.mapCargoDtoToCargo(cargoService.getCargoDTOByNumberGuide(detailsBlock.getNumberOfGuide()));
//        }
//        granite = graniteMapper.mapGraniteDtoToGranite(detailsBlock.getGranite());
//        graniteRepository.save(granite);
//        final Block block = blockMapper.mapBlockDtoToBlock(detailsBlock);
//        if(detailsBlock.getNumberOfGuide()!=null && !"".equals(detailsBlock.getNumberOfGuide())) {
//            block.setCargo(cargo);
//        }
//        return getDetailsBlockDTO(block, granite);
//    }

    //nao preciso deste id que esta a vir.... consigo ir buscar pelo details
    public BlockDTO updateBlock(final Integer id, final BlockDTO blockDTO) {
        Cargo cargo = new Cargo();
        if(blockDTO.getNumberOfGuide()!=null && !"".equals(blockDTO.getNumberOfGuide())) {
            cargo = cargoMapper.mapCargoDtoToCargo(cargoService.getCargoDTOByNumberGuide(blockDTO.getNumberOfGuide()));
        }
        final Block block = this.getBlockByIdFromRepository(id);
        block.setBlockNumber(blockDTO.getBlockNumber());
        block.setQuality(blockDTO.getQuality());
        block.setRating(blockDTO.getRating());
        block.setDescription(blockDTO.getDescription());
        block.setHeight(blockDTO.getHeight());
        block.setLength(blockDTO.getLength());
        block.setWidth(blockDTO.getWidth());
        if(blockDTO.getNumberOfGuide()!=null && !"".equals(blockDTO.getNumberOfGuide())) {
            block.setCargo(cargo);
        }
            blockRepository.save(block);
            return getBlockById(block.getId());
    }

//    public BlockDTO updateBlock2(final Integer id, final BlockDTO detailsBlock) {
//        final Block block = this.getBlockByIdFromRepository(id);
//        block.setBlockNumber(detailsBlock.getBlockNumber());
//        block.setQuality(detailsBlock.getQuality());
//        block.setRating(detailsBlock.getRating());
//        block.setDescription(detailsBlock.getDescription());
//        block.setHeight(detailsBlock.getHeight());
//        block.setLength(detailsBlock.getLength());
//        block.setWidth(detailsBlock.getWidth());
//        if (detailsBlock.getGranite().getId() == null) {
//            blockRepository.save(block);
//
//            return getBlockById(block.getId());
//        } else {
//            final Granite granite = this.getGraniteByIdFromRepository(detailsBlock.getGranite().getId());
//            granite.setFamily(detailsBlock.getGranite().getFamily());
//            granite.setType(detailsBlock.getGranite().getType());
//            granite.setPrice(detailsBlock.getGranite().getPrice());
//            graniteRepository.save(granite);
//
//            return getDetailsBlockDTO(block, granite);
//        }
//    }

    private BlockDTO getDetailsBlockDTO(final Block block, final Granite granite) {
        block.setGranite(granite);
        blockRepository.save(block);
//        detailsBlock
//                .setGranite(graniteMapper
//                        .mapGraniteToDto(getGraniteByIdFromRepository(granite.getId())));
//        detailsBlock
//                .setBlockDTO(blockMapper
//                        .mapBlockToDto(getBlockByIdFromRepository(block
//                                .getId())));
        return getBlockById(block.getId());
    }

    public Map<String, Boolean> deleteBlock(final Integer id) {
        final Block block = this.getBlockByIdFromRepository(id);
        blockRepository.delete(block);

        // TODO: Why return a Map? Wouldn't it be enough a boolean?
        final Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    private Block getBlockByIdFromRepository(final Integer id) {
        return blockRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Block not found on :: " + id));
    }

    private Granite getGraniteByIdFromRepository(final Integer id) {
        return graniteRepository
                .findById(id)
                .orElseThrow(() -> new NullPointerException("Block not found on :: " + id));
    }

    public List<BlockDTO> getBlockByIdCargo(final Integer cargoId) {
        final List<Block> blocks = blockRepository.findAllBlockByCargoId(cargoId);
        return blockMapper.mapBlocksToDto(blocks);
    }


//    public Float getBlockVolume(final Integer blockId) {
//        final Block block = this.getBlockByIdFromRepository(blockId);
//
//        return block.getHeight() * block.getLength() * block.getWidth();
//    }
}

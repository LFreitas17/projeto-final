package com.gra2003.backoffice.repository;

import com.gra2003.backoffice.model.Cargo;
import com.gra2003.backoffice.model.LoadState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CargoRepository extends JpaRepository<Cargo, Integer> {

    @Query("select p from cargo p where p.state = 'NORMAL' ")
    List<Cargo> findAllCargoesByState();

    @Query("select p from cargo p where p.state = :state ")
    List<Cargo> findAllCargoesByState2(@Param("state") LoadState state);

    @Query("select p from cargo p where p.numberGuide = :numberGuide ")
    Cargo findCargoByNumberGuide(@Param("numberGuide") String numberGuide);
}

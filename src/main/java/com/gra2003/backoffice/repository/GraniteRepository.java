package com.gra2003.backoffice.repository;

import com.gra2003.backoffice.model.Granite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GraniteRepository extends JpaRepository<Granite, Integer> {
}

package com.gra2003.backoffice.repository;

import com.gra2003.backoffice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query("select p from client_order p where p.name = :name ")
    Order findOrderByName(@Param("name") String name);

}

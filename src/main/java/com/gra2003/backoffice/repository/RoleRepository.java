package com.gra2003.backoffice.repository;

import com.gra2003.backoffice.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(final String name);
}

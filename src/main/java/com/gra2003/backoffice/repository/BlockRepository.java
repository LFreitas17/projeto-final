package com.gra2003.backoffice.repository;

import com.gra2003.backoffice.model.Block;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlockRepository extends JpaRepository<Block, Integer> {


    @Query("select p from block p where p.cargo.id = :cargoId ")
    List<Block> findAllBlockByCargoId(@Param("cargoId") Integer cargoId);
}

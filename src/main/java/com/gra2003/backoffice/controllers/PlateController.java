package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.model.Plate;
import com.gra2003.backoffice.repository.PlateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/plates")
@CrossOrigin(origins = "http://localhost:3000")
public class PlateController {

    @Autowired
    private PlateRepository plateRepository;


    @GetMapping
    public List<Plate> getAllPlates() {
        return plateRepository.findAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Plate> getPlateById(@PathVariable(value = "id") Integer plateId)
            throws NullPointerException {
        Plate plate =
                plateRepository
                        .findById(plateId)
                        .orElseThrow(() -> new NullPointerException("User not found on :: " + plateId));
        return ResponseEntity.ok().body(plate);
    }


    @PostMapping
    public Plate createPlate(@Valid @RequestBody Plate plate) {
        return plateRepository.save(plate);
    }
}

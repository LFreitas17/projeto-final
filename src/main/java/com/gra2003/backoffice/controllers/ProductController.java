package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.model.Product;
import com.gra2003.backoffice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
@CrossOrigin(origins = "http://localhost:3000")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable(value = "id") Integer productId)
            throws NullPointerException {
        Product product =
                productRepository
                        .findById(productId)
                        .orElseThrow(() -> new NullPointerException("User not found on :: " + productId));
        return ResponseEntity.ok().body(product);
    }


    @PostMapping
    public Product createProduct(@Valid @RequestBody Product product) {
        return productRepository.save(product);
    }
}

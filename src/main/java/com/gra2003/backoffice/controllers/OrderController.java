package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.dto.OrderDTO;
import com.gra2003.backoffice.model.Order;
import com.gra2003.backoffice.repository.OrderRepository;
import com.gra2003.backoffice.service.api.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/orders")
@CrossOrigin(origins = "http://localhost:3000")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<List<OrderDTO>> getAllSuppliers() {
        return new ResponseEntity<>(orderService.getAllOrder(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDTO> getSuppliersById(@PathVariable(value = "id") final Integer orderId) {
        return new ResponseEntity<>(orderService.getOrderById(orderId), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> createSupplier(@Valid @RequestBody final OrderDTO order) {
        return new ResponseEntity<>(orderService.createOrder(order), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> updateSupplier(@PathVariable(value = "id") final Integer orderId,
                                                      @Valid @RequestBody final OrderDTO orderDetails) {
        return new ResponseEntity<>(orderService.updateOrder(orderId, orderDetails), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteSupplier(@PathVariable(value = "id") final Integer orderId) {
        return new ResponseEntity<>(orderService.deleteOrder(orderId), HttpStatus.OK);
    }

}

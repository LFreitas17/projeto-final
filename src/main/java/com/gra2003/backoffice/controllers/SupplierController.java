package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.dto.SupplierDTO;
import com.gra2003.backoffice.model.Supplier;
import com.gra2003.backoffice.repository.SupplierRepository;
import com.gra2003.backoffice.service.api.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/suppliers")
@CrossOrigin(origins = "http://localhost:3000")
public class SupplierController {


    @Autowired
    private SupplierService supplierService;

    @GetMapping
    public ResponseEntity<List<SupplierDTO>> getAllSuppliers() {
        return new ResponseEntity<>(supplierService.getAllSupplier(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupplierDTO> getSuppliersById(@PathVariable(value = "id") final Integer supplierId) {
        return new ResponseEntity<>(supplierService.getSupplierById(supplierId), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SupplierDTO> createSupplier(@Valid @RequestBody final SupplierDTO supplier) {
        return new ResponseEntity<>(supplierService.createSupplier(supplier), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SupplierDTO> updateSupplier(@PathVariable(value = "id") final Integer supplierId,
                                              @Valid @RequestBody final SupplierDTO supplierDetails) {
        return new ResponseEntity<>(supplierService.updateSupplier(supplierId, supplierDetails), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteSupplier(@PathVariable(value = "id") final Integer supplierId) {
        return new ResponseEntity<>(supplierService.deleteSupplier(supplierId), HttpStatus.OK);
    }
}

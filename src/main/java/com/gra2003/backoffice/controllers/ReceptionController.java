package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.model.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/receptions")
@CrossOrigin(origins = "http://localhost:3000")
public class ReceptionController {


    @GetMapping("/enum/types")
    public List<LoadType> getAllLoadType() {

        final List<LoadType> types = new ArrayList<>();

        types.add(LoadType.EUROS_TON);
        types.add(LoadType.EUROS_MCUB);
        types.add(LoadType.EUROS_LOAD);

        return types;
    }

    @GetMapping("/enum/purchases")
    public List<PurchaseType> getAllPurchaseType() {

        final List<PurchaseType> purchases = new ArrayList<>();

        purchases.add(PurchaseType.BY_BLOCK);
        purchases.add(PurchaseType.BY_LOAD);

        return purchases;
    }

    @GetMapping("/enum/states")
    public List<LoadState> getAllLoadState() {

        final List<LoadState> states = new ArrayList<>();

        states.add(LoadState.NORMAL);
        states.add(LoadState.NOT_FINISHED);
        states.add(LoadState.FINISHED);
        states.add(LoadState.VERIFIED);

        return states;
    }

    @GetMapping("/enum/ratings")
    public List<Rating> getAllRating() {

        final List<Rating> ratings = new ArrayList<>();

        ratings.add(Rating.OK);
        ratings.add(Rating.NOT_OK);

        return ratings;
    }

    @GetMapping("/enum/qualities")
    public List<Quality> getAllQuality() {

        final List<Quality> qualities = new ArrayList<>();

        qualities.add(Quality.GOOD);
        qualities.add(Quality.BAD);

        return qualities;
    }


//    @GetMapping("/details/order")
//    public DetailOrder getAllInfoOrder() {
//
//
//        return DetailOrder;
//    }
// criar uma chamada que tem de trazer a informaçao da encomenda +  fornecedor + carga + blocos + granito por id da encomenda



}

package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.dto.GraniteDTO;
import com.gra2003.backoffice.service.api.GraniteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/granites")
@CrossOrigin(origins = "http://localhost:3000")
public class GraniteController {

    @Autowired
    private GraniteService graniteService;

    @GetMapping
    public ResponseEntity<List<GraniteDTO>> getAllGranites() {
        return new ResponseEntity<>(graniteService.getAllGranites(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GraniteDTO> getGranitesById(@PathVariable(value = "id") final Integer id) {
        return new ResponseEntity<>(graniteService.getGraniteById(id), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GraniteDTO> createGranite(@Valid @RequestBody final GraniteDTO granite) {
        return new ResponseEntity<>(graniteService.createGranite(granite), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GraniteDTO> updateGranite(@PathVariable(value = "id") final Integer graniteId,
                                                @Valid @RequestBody final GraniteDTO graniteDetails) {
        return new ResponseEntity<>(graniteService.updateGranite(graniteId, graniteDetails), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteGranite(@PathVariable(value = "id") final Integer graniteId) {
        return new ResponseEntity<>(graniteService.deleteGranite(graniteId), HttpStatus.OK);
    }
}

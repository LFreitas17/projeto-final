package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.dto.BlockDTO;
import com.gra2003.backoffice.service.api.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/blocks")
@CrossOrigin(origins = "http://localhost:3000")
public class BlockController {

    @Autowired
    private BlockService blockService;

    @GetMapping
    public ResponseEntity<List<BlockDTO>> getAllBlocks() {
        return new ResponseEntity<>(blockService.getAllBlocks(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BlockDTO> getBlockById(@PathVariable(value = "id") final Integer blockId) {
        return new ResponseEntity<>(blockService.getBlockById(blockId), HttpStatus.OK);
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BlockDTO> createBlock(@Valid @RequestBody final BlockDTO blockDetails) {
        return new ResponseEntity<>(blockService.createBlock(blockDetails), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BlockDTO> updateBlock(@PathVariable(value = "id") final Integer blockId,
                                              @Valid @RequestBody final BlockDTO blockDetails) {
        return new ResponseEntity<>(blockService.updateBlock(blockId, blockDetails), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteBlock(@PathVariable(value = "id") final Integer blockId) {
        return new ResponseEntity<>(blockService.deleteBlock(blockId), HttpStatus.OK);
    }

    @GetMapping("/block/{id}")
    public ResponseEntity<List<BlockDTO>> getBlockByCargoId(@PathVariable(value = "id") final Integer cargoId) {
        return new ResponseEntity<>(blockService.getBlockByIdCargo(cargoId), HttpStatus.OK);
    }

//    @GetMapping("volume/{id}")
//    public ResponseEntity<Integer> getVolumeOfBlocById(@PathVariable(value = "id") final Integer blockId) {
//        return new ResponseEntity<>(blockService.getBlockVolume(blockId), HttpStatus.OK);
//    }

//    @GetMapping("/price/{id}")
//    public Float getPriceOfBlocById(@PathVariable(value = "id") final Integer blockId){
//
//        return  blockDTO.priceOfBlock(blockId);
//    }
}

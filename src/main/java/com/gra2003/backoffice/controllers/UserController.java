package com.gra2003.backoffice.controllers;

import com.google.common.base.Strings;
import com.gra2003.backoffice.dto.UserDTO;
import com.gra2003.backoffice.service.api.UserService;
import com.gra2003.backoffice.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    /**
     * Get all users list.
     *
     * @return the list
     */
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    /**
     * Gets users by id.
     *
     * @param username the user username
     * @return the users by id
     */
    @GetMapping("/{username}")
    public ResponseEntity<UserDTO> getUserByUsername(@PathVariable(value = "username") final String username) {
        if (username == null || username.isEmpty()) {
            LOGGER.error("O username introduzido não é válido. O valor é null ou vazio!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        final UserDTO user;
        try {
            user = userService.findByUsername(username);
        } catch (final Exception ex) {
            LOGGER.error("Ocorreu um erro ao procurar o user: {}", ex.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * Create user user.
     *
     * @param user the user
     * @return the user
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUser(@Valid @RequestBody final UserDTO user, final BindingResult bindingResult) {
        if (!userService.loggedUserIsAdmin()) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        // TODO: Validate the input fields related to the user
        //userValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(error ->
                    LOGGER.error("Foi encontrado o seguinte erro ao criar o user: {}", error.getDefaultMessage()));
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            userService.createUser(user);
        } catch (final Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Update user response entity.
     *
     * @param userDetails the user details
     * @return the response entity
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUser(@Valid @RequestBody final UserDTO userDetails) {
        if (!userService.loggedUserIsAdmin()) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        if (userDetails == null || Strings.isNullOrEmpty(userDetails.getUsername())) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        try {
            userService.updateUser(userDetails);
        } catch (final Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Delete user map.
     *
     * @param username the username of the user to remove
     * @return the map
     */
    @DeleteMapping(value = "/{username}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteUser(@PathVariable(value = "username") final String username) {
        if (Strings.isNullOrEmpty(username)) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if (!userService.loggedUserIsAdmin() || username.equals(userService.getLoggedUsername())) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        try {
            userService.deleteUser(username);
        } catch (final Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

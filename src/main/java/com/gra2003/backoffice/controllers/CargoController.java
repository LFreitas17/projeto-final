package com.gra2003.backoffice.controllers;

import com.gra2003.backoffice.dto.CargoDTO;
import com.gra2003.backoffice.model.LoadState;
import com.gra2003.backoffice.service.api.CargoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/cargoes")
@CrossOrigin(origins = "http://localhost:3000")
public class CargoController {

    @Autowired
    private CargoService cargoService;

    @GetMapping
    public ResponseEntity<List<CargoDTO>> getAllCargoes() {
        return new ResponseEntity<>(cargoService.getAllCargoes(), HttpStatus.OK);
    }

    @GetMapping("/state")
    public ResponseEntity<List<CargoDTO>> getAllCargoesByState() {
        return new ResponseEntity<>(cargoService.getAllCargoesByState(), HttpStatus.OK);
    }

    @GetMapping("/state/{state}")
    public ResponseEntity<List<CargoDTO>> getAllCargoesByState2(@PathVariable(value = "state") final LoadState state) {
        return new ResponseEntity<>(cargoService.getAllCargoesByState2(state), HttpStatus.OK);
    }

    @PatchMapping(value = "/state/id/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CargoDTO> changeStateByCargoId(@PathVariable(value = "id") final Integer id) {
        return new ResponseEntity<>(cargoService.changeStateByCargoId(id), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CargoDTO> getCargoesById(@PathVariable(value = "id") final Integer id) {
        return new ResponseEntity<>(cargoService.getCargoById(id), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CargoDTO> createCargo(@Valid @RequestBody final CargoDTO cargoDTO) {
        return new ResponseEntity<>(cargoService.createCargo(cargoDTO), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CargoDTO> updateCargo(@PathVariable(value = "id") final Integer cargoId,
                                                @Valid @RequestBody final CargoDTO cargoDetails) {
        return new ResponseEntity<>(cargoService.updateCargo(cargoId, cargoDetails), HttpStatus.OK);
    }

    @PutMapping(value = "/order/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CargoDTO> updateCargoByAddOrder(@PathVariable(value = "id") final Integer orderId,
                                                @Valid @RequestBody final CargoDTO cargoDetails) {
        return new ResponseEntity<>(cargoService.updateCargoByAddOrder(orderId, cargoDetails), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteCargo(@PathVariable(value = "id") final Integer cargoId) {
        return new ResponseEntity<>(cargoService.deleteCargo(cargoId), HttpStatus.OK);
    }

    @GetMapping("/numberGuide/{numberGuide}")
    public ResponseEntity<Boolean> existsNumberGuide(@PathVariable(value = "numberGuide") final String numberGuide) {
        return new ResponseEntity<>(cargoService.existNumberOfGuide(numberGuide), HttpStatus.OK);
    }

}

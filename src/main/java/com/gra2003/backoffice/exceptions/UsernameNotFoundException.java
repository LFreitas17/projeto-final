package com.gra2003.backoffice.exceptions;

public class UsernameNotFoundException extends Exception {
    public UsernameNotFoundException(final String username) {
        super("O user " + username + " nao existe no sistema!");
    }
}

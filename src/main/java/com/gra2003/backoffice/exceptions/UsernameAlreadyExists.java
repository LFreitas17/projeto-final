package com.gra2003.backoffice.exceptions;

public class UsernameAlreadyExists extends Exception {
    public UsernameAlreadyExists(final String username) {
        super("O username " + username + " ja existe no sistema!");
    }
}

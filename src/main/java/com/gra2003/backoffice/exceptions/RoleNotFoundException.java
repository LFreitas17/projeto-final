package com.gra2003.backoffice.exceptions;

public class RoleNotFoundException extends Exception {
    public RoleNotFoundException(final String name) {
        super("O role " + name + " nao foi encontrado no sistema!");
    }
}
